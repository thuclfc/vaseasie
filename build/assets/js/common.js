/*!
 * project_shg
 * 
 * 
 * @author Thuclfc
 * @version 2.0.0
 * Copyright 2021. MIT licensed.
 */let product_path,
    product_opened = new Map(),
    orderTypeRequest,
    editByKeyRequest,
    loadingSpinner = '<div class="loadingio-spinner-spinner-x4w53rarava"><div class="ldio-2evcupwnrak"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
/**
 * Copyright (c) 2007-2015 Ariel Flesler - aflesler<a>gmail<d>com | http://flesler.blogspot.com
 * Licensed under MIT
 * @author Ariel Flesler
 * @version 2.1.2
 */

;

(function (f) {
  "use strict";

  "function" === typeof define && define.amd ? define(["jquery"], f) : "undefined" !== typeof module && module.exports ? module.exports = f(require("jquery")) : f(jQuery);
})(function ($) {
  "use strict";

  function n(a) {
    return !a.nodeName || -1 !== $.inArray(a.nodeName.toLowerCase(), ["iframe", "#document", "html", "body"]);
  }

  function h(a) {
    return $.isFunction(a) || $.isPlainObject(a) ? a : {
      top: a,
      left: a
    };
  }

  var p = $.scrollTo = function (a, d, b) {
    return $(window).scrollTo(a, d, b);
  };

  p.defaults = {
    axis: "xy",
    duration: 0,
    limit: !0
  };

  $.fn.scrollTo = function (a, d, b) {
    "object" === typeof d && (b = d, d = 0);
    "function" === typeof b && (b = {
      onAfter: b
    });
    "max" === a && (a = 9E9);
    b = $.extend({}, p.defaults, b);
    d = d || b.duration;
    var u = b.queue && 1 < b.axis.length;
    u && (d /= 2);
    b.offset = h(b.offset);
    b.over = h(b.over);
    return this.each(function () {
      function k(a) {
        var k = $.extend({}, b, {
          queue: !0,
          duration: d,
          complete: a && function () {
            a.call(q, e, b);
          }
        });
        r.animate(f, k);
      }

      if (null !== a) {
        var l = n(this),
            q = l ? this.contentWindow || window : this,
            r = $(q),
            e = a,
            f = {},
            t;

        switch (typeof e) {
          case "number":
          case "string":
            if (/^([+-]=?)?\d+(\.\d+)?(px|%)?$/.test(e)) {
              e = h(e);
              break;
            }

            e = l ? $(e) : $(e, q);

          case "object":
            if (e.length === 0) return;
            if (e.is || e.style) t = (e = $(e)).offset();
        }

        var v = $.isFunction(b.offset) && b.offset(q, e) || b.offset;
        $.each(b.axis.split(""), function (a, c) {
          var d = "x" === c ? "Left" : "Top",
              m = d.toLowerCase(),
              g = "scroll" + d,
              h = r[g](),
              n = p.max(q, c);
          t ? (f[g] = t[m] + (l ? 0 : h - r.offset()[m]), b.margin && (f[g] -= parseInt(e.css("margin" + d), 10) || 0, f[g] -= parseInt(e.css("border" + d + "Width"), 10) || 0), f[g] += v[m] || 0, b.over[m] && (f[g] += e["x" === c ? "width" : "height"]() * b.over[m])) : (d = e[m], f[g] = d.slice && "%" === d.slice(-1) ? parseFloat(d) / 100 * n : d);
          b.limit && /^\d+$/.test(f[g]) && (f[g] = 0 >= f[g] ? 0 : Math.min(f[g], n));
          !a && 1 < b.axis.length && (h === f[g] ? f = {} : u && (k(b.onAfterFirst), f = {}));
        });
        k(b.onAfter);
      }
    });
  };

  p.max = function (a, d) {
    var b = "x" === d ? "Width" : "Height",
        h = "scroll" + b;
    if (!n(a)) return a[h] - $(a)[b.toLowerCase()]();
    var b = "client" + b,
        k = a.ownerDocument || a.document,
        l = k.documentElement,
        k = k.body;
    return Math.max(l[h], k[h]) - Math.min(l[b], k[b]);
  };

  $.Tween.propHooks.scrollLeft = $.Tween.propHooks.scrollTop = {
    get: function (a) {
      return $(a.elem)[a.prop]();
    },
    set: function (a) {
      var d = this.get(a);
      if (a.options.interrupt && a._last && a._last !== d) return $(a.elem).stop();
      var b = Math.round(a.now);
      d !== b && ($(a.elem)[a.prop](b), a._last = this.get(a));
    }
  };
  return p;
});
/* perfect-scrollbar v0.7.1 */


!function t(e, n, r) {
  function o(i, s) {
    if (!n[i]) {
      if (!e[i]) {
        var a = "function" == typeof require && require;
        if (!s && a) return a(i, !0);
        if (l) return l(i, !0);
        var c = new Error("Cannot find module '" + i + "'");
        throw c.code = "MODULE_NOT_FOUND", c;
      }

      var u = n[i] = {
        exports: {}
      };
      e[i][0].call(u.exports, function (t) {
        var n = e[i][1][t];
        return o(n ? n : t);
      }, u, u.exports, t, e, n, r);
    }

    return n[i].exports;
  }

  for (var l = "function" == typeof require && require, i = 0; i < r.length; i++) o(r[i]);

  return o;
}({
  1: [function (t, e, n) {
    "use strict";

    function r(t) {
      t.fn.perfectScrollbar = function (t) {
        return this.each(function () {
          if ("object" == typeof t || "undefined" == typeof t) {
            var e = t;
            l.get(this) || o.initialize(this, e);
          } else {
            var n = t;
            "update" === n ? o.update(this) : "destroy" === n && o.destroy(this);
          }
        });
      };
    }

    var o = t("../main"),
        l = t("../plugin/instances");
    if ("function" == typeof define && define.amd) define(["jquery"], r);else {
      var i = window.jQuery ? window.jQuery : window.$;
      "undefined" != typeof i && r(i);
    }
    e.exports = r;
  }, {
    "../main": 7,
    "../plugin/instances": 18
  }],
  2: [function (t, e, n) {
    "use strict";

    function r(t, e) {
      var n = t.className.split(" ");
      n.indexOf(e) < 0 && n.push(e), t.className = n.join(" ");
    }

    function o(t, e) {
      var n = t.className.split(" "),
          r = n.indexOf(e);
      r >= 0 && n.splice(r, 1), t.className = n.join(" ");
    }

    n.add = function (t, e) {
      t.classList ? t.classList.add(e) : r(t, e);
    }, n.remove = function (t, e) {
      t.classList ? t.classList.remove(e) : o(t, e);
    }, n.list = function (t) {
      return t.classList ? Array.prototype.slice.apply(t.classList) : t.className.split(" ");
    };
  }, {}],
  3: [function (t, e, n) {
    "use strict";

    function r(t, e) {
      return window.getComputedStyle(t)[e];
    }

    function o(t, e, n) {
      return "number" == typeof n && (n = n.toString() + "px"), t.style[e] = n, t;
    }

    function l(t, e) {
      for (var n in e) {
        var r = e[n];
        "number" == typeof r && (r = r.toString() + "px"), t.style[n] = r;
      }

      return t;
    }

    var i = {};
    i.e = function (t, e) {
      var n = document.createElement(t);
      return n.className = e, n;
    }, i.appendTo = function (t, e) {
      return e.appendChild(t), t;
    }, i.css = function (t, e, n) {
      return "object" == typeof e ? l(t, e) : "undefined" == typeof n ? r(t, e) : o(t, e, n);
    }, i.matches = function (t, e) {
      return "undefined" != typeof t.matches ? t.matches(e) : "undefined" != typeof t.matchesSelector ? t.matchesSelector(e) : "undefined" != typeof t.webkitMatchesSelector ? t.webkitMatchesSelector(e) : "undefined" != typeof t.mozMatchesSelector ? t.mozMatchesSelector(e) : "undefined" != typeof t.msMatchesSelector ? t.msMatchesSelector(e) : void 0;
    }, i.remove = function (t) {
      "undefined" != typeof t.remove ? t.remove() : t.parentNode && t.parentNode.removeChild(t);
    }, i.queryChildren = function (t, e) {
      return Array.prototype.filter.call(t.childNodes, function (t) {
        return i.matches(t, e);
      });
    }, e.exports = i;
  }, {}],
  4: [function (t, e, n) {
    "use strict";

    var r = function (t) {
      this.element = t, this.events = {};
    };

    r.prototype.bind = function (t, e) {
      "undefined" == typeof this.events[t] && (this.events[t] = []), this.events[t].push(e), this.element.addEventListener(t, e, !1);
    }, r.prototype.unbind = function (t, e) {
      var n = "undefined" != typeof e;
      this.events[t] = this.events[t].filter(function (r) {
        return !(!n || r === e) || (this.element.removeEventListener(t, r, !1), !1);
      }, this);
    }, r.prototype.unbindAll = function () {
      for (var t in this.events) this.unbind(t);
    };

    var o = function () {
      this.eventElements = [];
    };

    o.prototype.eventElement = function (t) {
      var e = this.eventElements.filter(function (e) {
        return e.element === t;
      })[0];
      return "undefined" == typeof e && (e = new r(t), this.eventElements.push(e)), e;
    }, o.prototype.bind = function (t, e, n) {
      this.eventElement(t).bind(e, n);
    }, o.prototype.unbind = function (t, e, n) {
      this.eventElement(t).unbind(e, n);
    }, o.prototype.unbindAll = function () {
      for (var t = 0; t < this.eventElements.length; t++) this.eventElements[t].unbindAll();
    }, o.prototype.once = function (t, e, n) {
      var r = this.eventElement(t),
          o = function (t) {
        r.unbind(e, o), n(t);
      };

      r.bind(e, o);
    }, e.exports = o;
  }, {}],
  5: [function (t, e, n) {
    "use strict";

    e.exports = function () {
      function t() {
        return Math.floor(65536 * (1 + Math.random())).toString(16).substring(1);
      }

      return function () {
        return t() + t() + "-" + t() + "-" + t() + "-" + t() + "-" + t() + t() + t();
      };
    }();
  }, {}],
  6: [function (t, e, n) {
    "use strict";

    function r(t) {
      return function (e, n) {
        t(e, "ps--in-scrolling"), "undefined" != typeof n ? t(e, "ps--" + n) : (t(e, "ps--x"), t(e, "ps--y"));
      };
    }

    var o = t("./class"),
        l = t("./dom"),
        i = n.toInt = function (t) {
      return parseInt(t, 10) || 0;
    },
        s = n.clone = function (t) {
      if (t) {
        if (Array.isArray(t)) return t.map(s);

        if ("object" == typeof t) {
          var e = {};

          for (var n in t) e[n] = s(t[n]);

          return e;
        }

        return t;
      }

      return null;
    };

    n.extend = function (t, e) {
      var n = s(t);

      for (var r in e) n[r] = s(e[r]);

      return n;
    }, n.isEditable = function (t) {
      return l.matches(t, "input,[contenteditable]") || l.matches(t, "select,[contenteditable]") || l.matches(t, "textarea,[contenteditable]") || l.matches(t, "button,[contenteditable]");
    }, n.removePsClasses = function (t) {
      for (var e = o.list(t), n = 0; n < e.length; n++) {
        var r = e[n];
        0 === r.indexOf("ps-") && o.remove(t, r);
      }
    }, n.outerWidth = function (t) {
      return i(l.css(t, "width")) + i(l.css(t, "paddingLeft")) + i(l.css(t, "paddingRight")) + i(l.css(t, "borderLeftWidth")) + i(l.css(t, "borderRightWidth"));
    }, n.startScrolling = r(o.add), n.stopScrolling = r(o.remove), n.env = {
      isWebKit: "undefined" != typeof document && "WebkitAppearance" in document.documentElement.style,
      supportsTouch: "undefined" != typeof window && ("ontouchstart" in window || window.DocumentTouch && document instanceof window.DocumentTouch),
      supportsIePointer: "undefined" != typeof window && null !== window.navigator.msMaxTouchPoints
    };
  }, {
    "./class": 2,
    "./dom": 3
  }],
  7: [function (t, e, n) {
    "use strict";

    var r = t("./plugin/destroy"),
        o = t("./plugin/initialize"),
        l = t("./plugin/update");
    e.exports = {
      initialize: o,
      update: l,
      destroy: r
    };
  }, {
    "./plugin/destroy": 9,
    "./plugin/initialize": 17,
    "./plugin/update": 21
  }],
  8: [function (t, e, n) {
    "use strict";

    e.exports = {
      handlers: ["click-rail", "drag-scrollbar", "keyboard", "wheel", "touch"],
      maxScrollbarLength: null,
      minScrollbarLength: null,
      scrollXMarginOffset: 0,
      scrollYMarginOffset: 0,
      suppressScrollX: !1,
      suppressScrollY: !1,
      swipePropagation: !0,
      swipeEasing: !0,
      useBothWheelAxes: !1,
      wheelPropagation: !1,
      wheelSpeed: 1,
      theme: "default"
    };
  }, {}],
  9: [function (t, e, n) {
    "use strict";

    var r = t("../lib/helper"),
        o = t("../lib/dom"),
        l = t("./instances");

    e.exports = function (t) {
      var e = l.get(t);
      e && (e.event.unbindAll(), o.remove(e.scrollbarX), o.remove(e.scrollbarY), o.remove(e.scrollbarXRail), o.remove(e.scrollbarYRail), r.removePsClasses(t), l.remove(t));
    };
  }, {
    "../lib/dom": 3,
    "../lib/helper": 6,
    "./instances": 18
  }],
  10: [function (t, e, n) {
    "use strict";

    function r(t, e) {
      function n(t) {
        return t.getBoundingClientRect();
      }

      var r = function (t) {
        t.stopPropagation();
      };

      e.event.bind(e.scrollbarY, "click", r), e.event.bind(e.scrollbarYRail, "click", function (r) {
        var o = r.pageY - window.pageYOffset - n(e.scrollbarYRail).top,
            s = o > e.scrollbarYTop ? 1 : -1;
        i(t, "top", t.scrollTop + s * e.containerHeight), l(t), r.stopPropagation();
      }), e.event.bind(e.scrollbarX, "click", r), e.event.bind(e.scrollbarXRail, "click", function (r) {
        var o = r.pageX - window.pageXOffset - n(e.scrollbarXRail).left,
            s = o > e.scrollbarXLeft ? 1 : -1;
        i(t, "left", t.scrollLeft + s * e.containerWidth), l(t), r.stopPropagation();
      });
    }

    var o = t("../instances"),
        l = t("../update-geometry"),
        i = t("../update-scroll");

    e.exports = function (t) {
      var e = o.get(t);
      r(t, e);
    };
  }, {
    "../instances": 18,
    "../update-geometry": 19,
    "../update-scroll": 20
  }],
  11: [function (t, e, n) {
    "use strict";

    function r(t, e) {
      function n(n) {
        var o = r + n * e.railXRatio,
            i = Math.max(0, e.scrollbarXRail.getBoundingClientRect().left) + e.railXRatio * (e.railXWidth - e.scrollbarXWidth);
        o < 0 ? e.scrollbarXLeft = 0 : o > i ? e.scrollbarXLeft = i : e.scrollbarXLeft = o;
        var s = l.toInt(e.scrollbarXLeft * (e.contentWidth - e.containerWidth) / (e.containerWidth - e.railXRatio * e.scrollbarXWidth)) - e.negativeScrollAdjustment;
        c(t, "left", s);
      }

      var r = null,
          o = null,
          s = function (e) {
        n(e.pageX - o), a(t), e.stopPropagation(), e.preventDefault();
      },
          u = function () {
        l.stopScrolling(t, "x"), e.event.unbind(e.ownerDocument, "mousemove", s);
      };

      e.event.bind(e.scrollbarX, "mousedown", function (n) {
        o = n.pageX, r = l.toInt(i.css(e.scrollbarX, "left")) * e.railXRatio, l.startScrolling(t, "x"), e.event.bind(e.ownerDocument, "mousemove", s), e.event.once(e.ownerDocument, "mouseup", u), n.stopPropagation(), n.preventDefault();
      });
    }

    function o(t, e) {
      function n(n) {
        var o = r + n * e.railYRatio,
            i = Math.max(0, e.scrollbarYRail.getBoundingClientRect().top) + e.railYRatio * (e.railYHeight - e.scrollbarYHeight);
        o < 0 ? e.scrollbarYTop = 0 : o > i ? e.scrollbarYTop = i : e.scrollbarYTop = o;
        var s = l.toInt(e.scrollbarYTop * (e.contentHeight - e.containerHeight) / (e.containerHeight - e.railYRatio * e.scrollbarYHeight));
        c(t, "top", s);
      }

      var r = null,
          o = null,
          s = function (e) {
        n(e.pageY - o), a(t), e.stopPropagation(), e.preventDefault();
      },
          u = function () {
        l.stopScrolling(t, "y"), e.event.unbind(e.ownerDocument, "mousemove", s);
      };

      e.event.bind(e.scrollbarY, "mousedown", function (n) {
        o = n.pageY, r = l.toInt(i.css(e.scrollbarY, "top")) * e.railYRatio, l.startScrolling(t, "y"), e.event.bind(e.ownerDocument, "mousemove", s), e.event.once(e.ownerDocument, "mouseup", u), n.stopPropagation(), n.preventDefault();
      });
    }

    var l = t("../../lib/helper"),
        i = t("../../lib/dom"),
        s = t("../instances"),
        a = t("../update-geometry"),
        c = t("../update-scroll");

    e.exports = function (t) {
      var e = s.get(t);
      r(t, e), o(t, e);
    };
  }, {
    "../../lib/dom": 3,
    "../../lib/helper": 6,
    "../instances": 18,
    "../update-geometry": 19,
    "../update-scroll": 20
  }],
  12: [function (t, e, n) {
    "use strict";

    function r(t, e) {
      function n(n, r) {
        var o = t.scrollTop;

        if (0 === n) {
          if (!e.scrollbarYActive) return !1;
          if (0 === o && r > 0 || o >= e.contentHeight - e.containerHeight && r < 0) return !e.settings.wheelPropagation;
        }

        var l = t.scrollLeft;

        if (0 === r) {
          if (!e.scrollbarXActive) return !1;
          if (0 === l && n < 0 || l >= e.contentWidth - e.containerWidth && n > 0) return !e.settings.wheelPropagation;
        }

        return !0;
      }

      var r = !1;
      e.event.bind(t, "mouseenter", function () {
        r = !0;
      }), e.event.bind(t, "mouseleave", function () {
        r = !1;
      });
      var i = !1;
      e.event.bind(e.ownerDocument, "keydown", function (c) {
        if (!(c.isDefaultPrevented && c.isDefaultPrevented() || c.defaultPrevented)) {
          var u = l.matches(e.scrollbarX, ":focus") || l.matches(e.scrollbarY, ":focus");

          if (r || u) {
            var d = document.activeElement ? document.activeElement : e.ownerDocument.activeElement;

            if (d) {
              if ("IFRAME" === d.tagName) d = d.contentDocument.activeElement;else for (; d.shadowRoot;) d = d.shadowRoot.activeElement;
              if (o.isEditable(d)) return;
            }

            var p = 0,
                f = 0;

            switch (c.which) {
              case 37:
                p = c.metaKey ? -e.contentWidth : c.altKey ? -e.containerWidth : -30;
                break;

              case 38:
                f = c.metaKey ? e.contentHeight : c.altKey ? e.containerHeight : 30;
                break;

              case 39:
                p = c.metaKey ? e.contentWidth : c.altKey ? e.containerWidth : 30;
                break;

              case 40:
                f = c.metaKey ? -e.contentHeight : c.altKey ? -e.containerHeight : -30;
                break;

              case 33:
                f = 90;
                break;

              case 32:
                f = c.shiftKey ? 90 : -90;
                break;

              case 34:
                f = -90;
                break;

              case 35:
                f = c.ctrlKey ? -e.contentHeight : -e.containerHeight;
                break;

              case 36:
                f = c.ctrlKey ? t.scrollTop : e.containerHeight;
                break;

              default:
                return;
            }

            a(t, "top", t.scrollTop - f), a(t, "left", t.scrollLeft + p), s(t), i = n(p, f), i && c.preventDefault();
          }
        }
      });
    }

    var o = t("../../lib/helper"),
        l = t("../../lib/dom"),
        i = t("../instances"),
        s = t("../update-geometry"),
        a = t("../update-scroll");

    e.exports = function (t) {
      var e = i.get(t);
      r(t, e);
    };
  }, {
    "../../lib/dom": 3,
    "../../lib/helper": 6,
    "../instances": 18,
    "../update-geometry": 19,
    "../update-scroll": 20
  }],
  13: [function (t, e, n) {
    "use strict";

    function r(t, e) {
      function n(n, r) {
        var o = t.scrollTop;

        if (0 === n) {
          if (!e.scrollbarYActive) return !1;
          if (0 === o && r > 0 || o >= e.contentHeight - e.containerHeight && r < 0) return !e.settings.wheelPropagation;
        }

        var l = t.scrollLeft;

        if (0 === r) {
          if (!e.scrollbarXActive) return !1;
          if (0 === l && n < 0 || l >= e.contentWidth - e.containerWidth && n > 0) return !e.settings.wheelPropagation;
        }

        return !0;
      }

      function r(t) {
        var e = t.deltaX,
            n = -1 * t.deltaY;
        return "undefined" != typeof e && "undefined" != typeof n || (e = -1 * t.wheelDeltaX / 6, n = t.wheelDeltaY / 6), t.deltaMode && 1 === t.deltaMode && (e *= 10, n *= 10), e !== e && n !== n && (e = 0, n = t.wheelDelta), t.shiftKey ? [-n, -e] : [e, n];
      }

      function o(e, n) {
        var r = t.querySelector("textarea:hover, select[multiple]:hover, .ps-child:hover");

        if (r) {
          var o = window.getComputedStyle(r),
              l = [o.overflow, o.overflowX, o.overflowY].join("");
          if (!l.match(/(scroll|auto)/)) return !1;
          var i = r.scrollHeight - r.clientHeight;
          if (i > 0 && !(0 === r.scrollTop && n > 0 || r.scrollTop === i && n < 0)) return !0;
          var s = r.scrollLeft - r.clientWidth;
          if (s > 0 && !(0 === r.scrollLeft && e < 0 || r.scrollLeft === s && e > 0)) return !0;
        }

        return !1;
      }

      function s(s) {
        var c = r(s),
            u = c[0],
            d = c[1];
        o(u, d) || (a = !1, e.settings.useBothWheelAxes ? e.scrollbarYActive && !e.scrollbarXActive ? (d ? i(t, "top", t.scrollTop - d * e.settings.wheelSpeed) : i(t, "top", t.scrollTop + u * e.settings.wheelSpeed), a = !0) : e.scrollbarXActive && !e.scrollbarYActive && (u ? i(t, "left", t.scrollLeft + u * e.settings.wheelSpeed) : i(t, "left", t.scrollLeft - d * e.settings.wheelSpeed), a = !0) : (i(t, "top", t.scrollTop - d * e.settings.wheelSpeed), i(t, "left", t.scrollLeft + u * e.settings.wheelSpeed)), l(t), a = a || n(u, d), a && (s.stopPropagation(), s.preventDefault()));
      }

      var a = !1;
      "undefined" != typeof window.onwheel ? e.event.bind(t, "wheel", s) : "undefined" != typeof window.onmousewheel && e.event.bind(t, "mousewheel", s);
    }

    var o = t("../instances"),
        l = t("../update-geometry"),
        i = t("../update-scroll");

    e.exports = function (t) {
      var e = o.get(t);
      r(t, e);
    };
  }, {
    "../instances": 18,
    "../update-geometry": 19,
    "../update-scroll": 20
  }],
  14: [function (t, e, n) {
    "use strict";

    function r(t, e) {
      e.event.bind(t, "scroll", function () {
        l(t);
      });
    }

    var o = t("../instances"),
        l = t("../update-geometry");

    e.exports = function (t) {
      var e = o.get(t);
      r(t, e);
    };
  }, {
    "../instances": 18,
    "../update-geometry": 19
  }],
  15: [function (t, e, n) {
    "use strict";

    function r(t, e) {
      function n() {
        var t = window.getSelection ? window.getSelection() : document.getSelection ? document.getSelection() : "";
        return 0 === t.toString().length ? null : t.getRangeAt(0).commonAncestorContainer;
      }

      function r() {
        c || (c = setInterval(function () {
          return l.get(t) ? (s(t, "top", t.scrollTop + u.top), s(t, "left", t.scrollLeft + u.left), void i(t)) : void clearInterval(c);
        }, 50));
      }

      function a() {
        c && (clearInterval(c), c = null), o.stopScrolling(t);
      }

      var c = null,
          u = {
        top: 0,
        left: 0
      },
          d = !1;
      e.event.bind(e.ownerDocument, "selectionchange", function () {
        t.contains(n()) ? d = !0 : (d = !1, a());
      }), e.event.bind(window, "mouseup", function () {
        d && (d = !1, a());
      }), e.event.bind(window, "keyup", function () {
        d && (d = !1, a());
      }), e.event.bind(window, "mousemove", function (e) {
        if (d) {
          var n = {
            x: e.pageX,
            y: e.pageY
          },
              l = {
            left: t.offsetLeft,
            right: t.offsetLeft + t.offsetWidth,
            top: t.offsetTop,
            bottom: t.offsetTop + t.offsetHeight
          };
          n.x < l.left + 3 ? (u.left = -5, o.startScrolling(t, "x")) : n.x > l.right - 3 ? (u.left = 5, o.startScrolling(t, "x")) : u.left = 0, n.y < l.top + 3 ? (l.top + 3 - n.y < 5 ? u.top = -5 : u.top = -20, o.startScrolling(t, "y")) : n.y > l.bottom - 3 ? (n.y - l.bottom + 3 < 5 ? u.top = 5 : u.top = 20, o.startScrolling(t, "y")) : u.top = 0, 0 === u.top && 0 === u.left ? a() : r();
        }
      });
    }

    var o = t("../../lib/helper"),
        l = t("../instances"),
        i = t("../update-geometry"),
        s = t("../update-scroll");

    e.exports = function (t) {
      var e = l.get(t);
      r(t, e);
    };
  }, {
    "../../lib/helper": 6,
    "../instances": 18,
    "../update-geometry": 19,
    "../update-scroll": 20
  }],
  16: [function (t, e, n) {
    "use strict";

    function r(t, e, n, r) {
      function o(n, r) {
        var o = t.scrollTop,
            l = t.scrollLeft,
            i = Math.abs(n),
            s = Math.abs(r);

        if (s > i) {
          if (r < 0 && o === e.contentHeight - e.containerHeight || r > 0 && 0 === o) return !e.settings.swipePropagation;
        } else if (i > s && (n < 0 && l === e.contentWidth - e.containerWidth || n > 0 && 0 === l)) return !e.settings.swipePropagation;

        return !0;
      }

      function a(e, n) {
        s(t, "top", t.scrollTop - n), s(t, "left", t.scrollLeft - e), i(t);
      }

      function c() {
        w = !0;
      }

      function u() {
        w = !1;
      }

      function d(t) {
        return t.targetTouches ? t.targetTouches[0] : t;
      }

      function p(t) {
        return !(!t.targetTouches || 1 !== t.targetTouches.length) || !(!t.pointerType || "mouse" === t.pointerType || t.pointerType === t.MSPOINTER_TYPE_MOUSE);
      }

      function f(t) {
        if (p(t)) {
          Y = !0;
          var e = d(t);
          g.pageX = e.pageX, g.pageY = e.pageY, v = new Date().getTime(), null !== y && clearInterval(y), t.stopPropagation();
        }
      }

      function h(t) {
        if (!Y && e.settings.swipePropagation && f(t), !w && Y && p(t)) {
          var n = d(t),
              r = {
            pageX: n.pageX,
            pageY: n.pageY
          },
              l = r.pageX - g.pageX,
              i = r.pageY - g.pageY;
          a(l, i), g = r;
          var s = new Date().getTime(),
              c = s - v;
          c > 0 && (m.x = l / c, m.y = i / c, v = s), o(l, i) && (t.stopPropagation(), t.preventDefault());
        }
      }

      function b() {
        !w && Y && (Y = !1, e.settings.swipeEasing && (clearInterval(y), y = setInterval(function () {
          return l.get(t) && (m.x || m.y) ? Math.abs(m.x) < .01 && Math.abs(m.y) < .01 ? void clearInterval(y) : (a(30 * m.x, 30 * m.y), m.x *= .8, void (m.y *= .8)) : void clearInterval(y);
        }, 10)));
      }

      var g = {},
          v = 0,
          m = {},
          y = null,
          w = !1,
          Y = !1;
      n ? (e.event.bind(window, "touchstart", c), e.event.bind(window, "touchend", u), e.event.bind(t, "touchstart", f), e.event.bind(t, "touchmove", h), e.event.bind(t, "touchend", b)) : r && (window.PointerEvent ? (e.event.bind(window, "pointerdown", c), e.event.bind(window, "pointerup", u), e.event.bind(t, "pointerdown", f), e.event.bind(t, "pointermove", h), e.event.bind(t, "pointerup", b)) : window.MSPointerEvent && (e.event.bind(window, "MSPointerDown", c), e.event.bind(window, "MSPointerUp", u), e.event.bind(t, "MSPointerDown", f), e.event.bind(t, "MSPointerMove", h), e.event.bind(t, "MSPointerUp", b)));
    }

    var o = t("../../lib/helper"),
        l = t("../instances"),
        i = t("../update-geometry"),
        s = t("../update-scroll");

    e.exports = function (t) {
      if (o.env.supportsTouch || o.env.supportsIePointer) {
        var e = l.get(t);
        r(t, e, o.env.supportsTouch, o.env.supportsIePointer);
      }
    };
  }, {
    "../../lib/helper": 6,
    "../instances": 18,
    "../update-geometry": 19,
    "../update-scroll": 20
  }],
  17: [function (t, e, n) {
    "use strict";

    var r = t("../lib/helper"),
        o = t("../lib/class"),
        l = t("./instances"),
        i = t("./update-geometry"),
        s = {
      "click-rail": t("./handler/click-rail"),
      "drag-scrollbar": t("./handler/drag-scrollbar"),
      keyboard: t("./handler/keyboard"),
      wheel: t("./handler/mouse-wheel"),
      touch: t("./handler/touch"),
      selection: t("./handler/selection")
    },
        a = t("./handler/native-scroll");

    e.exports = function (t, e) {
      e = "object" == typeof e ? e : {}, o.add(t, "ps");
      var n = l.add(t);
      n.settings = r.extend(n.settings, e), o.add(t, "ps--theme_" + n.settings.theme), n.settings.handlers.forEach(function (e) {
        s[e](t);
      }), a(t), i(t);
    };
  }, {
    "../lib/class": 2,
    "../lib/helper": 6,
    "./handler/click-rail": 10,
    "./handler/drag-scrollbar": 11,
    "./handler/keyboard": 12,
    "./handler/mouse-wheel": 13,
    "./handler/native-scroll": 14,
    "./handler/selection": 15,
    "./handler/touch": 16,
    "./instances": 18,
    "./update-geometry": 19
  }],
  18: [function (t, e, n) {
    "use strict";

    function r(t) {
      function e() {
        a.add(t, "ps--focus");
      }

      function n() {
        a.remove(t, "ps--focus");
      }

      var r = this;
      r.settings = s.clone(c), r.containerWidth = null, r.containerHeight = null, r.contentWidth = null, r.contentHeight = null, r.isRtl = "rtl" === u.css(t, "direction"), r.isNegativeScroll = function () {
        var e = t.scrollLeft,
            n = null;
        return t.scrollLeft = -1, n = t.scrollLeft < 0, t.scrollLeft = e, n;
      }(), r.negativeScrollAdjustment = r.isNegativeScroll ? t.scrollWidth - t.clientWidth : 0, r.event = new d(), r.ownerDocument = t.ownerDocument || document, r.scrollbarXRail = u.appendTo(u.e("div", "ps__scrollbar-x-rail"), t), r.scrollbarX = u.appendTo(u.e("div", "ps__scrollbar-x"), r.scrollbarXRail), r.scrollbarX.setAttribute("tabindex", 0), r.event.bind(r.scrollbarX, "focus", e), r.event.bind(r.scrollbarX, "blur", n), r.scrollbarXActive = null, r.scrollbarXWidth = null, r.scrollbarXLeft = null, r.scrollbarXBottom = s.toInt(u.css(r.scrollbarXRail, "bottom")), r.isScrollbarXUsingBottom = r.scrollbarXBottom === r.scrollbarXBottom, r.scrollbarXTop = r.isScrollbarXUsingBottom ? null : s.toInt(u.css(r.scrollbarXRail, "top")), r.railBorderXWidth = s.toInt(u.css(r.scrollbarXRail, "borderLeftWidth")) + s.toInt(u.css(r.scrollbarXRail, "borderRightWidth")), u.css(r.scrollbarXRail, "display", "block"), r.railXMarginWidth = s.toInt(u.css(r.scrollbarXRail, "marginLeft")) + s.toInt(u.css(r.scrollbarXRail, "marginRight")), u.css(r.scrollbarXRail, "display", ""), r.railXWidth = null, r.railXRatio = null, r.scrollbarYRail = u.appendTo(u.e("div", "ps__scrollbar-y-rail"), t), r.scrollbarY = u.appendTo(u.e("div", "ps__scrollbar-y"), r.scrollbarYRail), r.scrollbarY.setAttribute("tabindex", 0), r.event.bind(r.scrollbarY, "focus", e), r.event.bind(r.scrollbarY, "blur", n), r.scrollbarYActive = null, r.scrollbarYHeight = null, r.scrollbarYTop = null, r.scrollbarYRight = s.toInt(u.css(r.scrollbarYRail, "right")), r.isScrollbarYUsingRight = r.scrollbarYRight === r.scrollbarYRight, r.scrollbarYLeft = r.isScrollbarYUsingRight ? null : s.toInt(u.css(r.scrollbarYRail, "left")), r.scrollbarYOuterWidth = r.isRtl ? s.outerWidth(r.scrollbarY) : null, r.railBorderYWidth = s.toInt(u.css(r.scrollbarYRail, "borderTopWidth")) + s.toInt(u.css(r.scrollbarYRail, "borderBottomWidth")), u.css(r.scrollbarYRail, "display", "block"), r.railYMarginHeight = s.toInt(u.css(r.scrollbarYRail, "marginTop")) + s.toInt(u.css(r.scrollbarYRail, "marginBottom")), u.css(r.scrollbarYRail, "display", ""), r.railYHeight = null, r.railYRatio = null;
    }

    function o(t) {
      return t.getAttribute("data-ps-id");
    }

    function l(t, e) {
      t.setAttribute("data-ps-id", e);
    }

    function i(t) {
      t.removeAttribute("data-ps-id");
    }

    var s = t("../lib/helper"),
        a = t("../lib/class"),
        c = t("./default-setting"),
        u = t("../lib/dom"),
        d = t("../lib/event-manager"),
        p = t("../lib/guid"),
        f = {};
    n.add = function (t) {
      var e = p();
      return l(t, e), f[e] = new r(t), f[e];
    }, n.remove = function (t) {
      delete f[o(t)], i(t);
    }, n.get = function (t) {
      return f[o(t)];
    };
  }, {
    "../lib/class": 2,
    "../lib/dom": 3,
    "../lib/event-manager": 4,
    "../lib/guid": 5,
    "../lib/helper": 6,
    "./default-setting": 8
  }],
  19: [function (t, e, n) {
    "use strict";

    function r(t, e) {
      return t.settings.minScrollbarLength && (e = Math.max(e, t.settings.minScrollbarLength)), t.settings.maxScrollbarLength && (e = Math.min(e, t.settings.maxScrollbarLength)), e;
    }

    function o(t, e) {
      var n = {
        width: e.railXWidth
      };
      e.isRtl ? n.left = e.negativeScrollAdjustment + t.scrollLeft + e.containerWidth - e.contentWidth : n.left = t.scrollLeft, e.isScrollbarXUsingBottom ? n.bottom = e.scrollbarXBottom - t.scrollTop : n.top = e.scrollbarXTop + t.scrollTop, s.css(e.scrollbarXRail, n);
      var r = {
        top: t.scrollTop,
        height: e.railYHeight
      };
      e.isScrollbarYUsingRight ? e.isRtl ? r.right = e.contentWidth - (e.negativeScrollAdjustment + t.scrollLeft) - e.scrollbarYRight - e.scrollbarYOuterWidth : r.right = e.scrollbarYRight - t.scrollLeft : e.isRtl ? r.left = e.negativeScrollAdjustment + t.scrollLeft + 2 * e.containerWidth - e.contentWidth - e.scrollbarYLeft - e.scrollbarYOuterWidth : r.left = e.scrollbarYLeft + t.scrollLeft, s.css(e.scrollbarYRail, r), s.css(e.scrollbarX, {
        left: e.scrollbarXLeft,
        width: e.scrollbarXWidth - e.railBorderXWidth
      }), s.css(e.scrollbarY, {
        top: e.scrollbarYTop,
        height: e.scrollbarYHeight - e.railBorderYWidth
      });
    }

    var l = t("../lib/helper"),
        i = t("../lib/class"),
        s = t("../lib/dom"),
        a = t("./instances"),
        c = t("./update-scroll");

    e.exports = function (t) {
      var e = a.get(t);
      e.containerWidth = t.clientWidth, e.containerHeight = t.clientHeight, e.contentWidth = t.scrollWidth, e.contentHeight = t.scrollHeight;
      var n;
      t.contains(e.scrollbarXRail) || (n = s.queryChildren(t, ".ps__scrollbar-x-rail"), n.length > 0 && n.forEach(function (t) {
        s.remove(t);
      }), s.appendTo(e.scrollbarXRail, t)), t.contains(e.scrollbarYRail) || (n = s.queryChildren(t, ".ps__scrollbar-y-rail"), n.length > 0 && n.forEach(function (t) {
        s.remove(t);
      }), s.appendTo(e.scrollbarYRail, t)), !e.settings.suppressScrollX && e.containerWidth + e.settings.scrollXMarginOffset < e.contentWidth ? (e.scrollbarXActive = !0, e.railXWidth = e.containerWidth - e.railXMarginWidth, e.railXRatio = e.containerWidth / e.railXWidth, e.scrollbarXWidth = r(e, l.toInt(e.railXWidth * e.containerWidth / e.contentWidth)), e.scrollbarXLeft = l.toInt((e.negativeScrollAdjustment + t.scrollLeft) * (e.railXWidth - e.scrollbarXWidth) / (e.contentWidth - e.containerWidth))) : e.scrollbarXActive = !1, !e.settings.suppressScrollY && e.containerHeight + e.settings.scrollYMarginOffset < e.contentHeight ? (e.scrollbarYActive = !0, e.railYHeight = e.containerHeight - e.railYMarginHeight, e.railYRatio = e.containerHeight / e.railYHeight, e.scrollbarYHeight = r(e, l.toInt(e.railYHeight * e.containerHeight / e.contentHeight)), e.scrollbarYTop = l.toInt(t.scrollTop * (e.railYHeight - e.scrollbarYHeight) / (e.contentHeight - e.containerHeight))) : e.scrollbarYActive = !1, e.scrollbarXLeft >= e.railXWidth - e.scrollbarXWidth && (e.scrollbarXLeft = e.railXWidth - e.scrollbarXWidth), e.scrollbarYTop >= e.railYHeight - e.scrollbarYHeight && (e.scrollbarYTop = e.railYHeight - e.scrollbarYHeight), o(t, e), e.scrollbarXActive ? i.add(t, "ps--active-x") : (i.remove(t, "ps--active-x"), e.scrollbarXWidth = 0, e.scrollbarXLeft = 0, c(t, "left", 0)), e.scrollbarYActive ? i.add(t, "ps--active-y") : (i.remove(t, "ps--active-y"), e.scrollbarYHeight = 0, e.scrollbarYTop = 0, c(t, "top", 0));
    };
  }, {
    "../lib/class": 2,
    "../lib/dom": 3,
    "../lib/helper": 6,
    "./instances": 18,
    "./update-scroll": 20
  }],
  20: [function (t, e, n) {
    "use strict";

    var r = t("./instances"),
        o = function (t) {
      var e = document.createEvent("Event");
      return e.initEvent(t, !0, !0), e;
    };

    e.exports = function (t, e, n) {
      if ("undefined" == typeof t) throw "You must provide an element to the update-scroll function";
      if ("undefined" == typeof e) throw "You must provide an axis to the update-scroll function";
      if ("undefined" == typeof n) throw "You must provide a value to the update-scroll function";
      "top" === e && n <= 0 && (t.scrollTop = n = 0, t.dispatchEvent(o("ps-y-reach-start"))), "left" === e && n <= 0 && (t.scrollLeft = n = 0, t.dispatchEvent(o("ps-x-reach-start")));
      var l = r.get(t);
      "top" === e && n >= l.contentHeight - l.containerHeight && (n = l.contentHeight - l.containerHeight, n - t.scrollTop <= 1 ? n = t.scrollTop : t.scrollTop = n, t.dispatchEvent(o("ps-y-reach-end"))), "left" === e && n >= l.contentWidth - l.containerWidth && (n = l.contentWidth - l.containerWidth, n - t.scrollLeft <= 1 ? n = t.scrollLeft : t.scrollLeft = n, t.dispatchEvent(o("ps-x-reach-end"))), void 0 === l.lastTop && (l.lastTop = t.scrollTop), void 0 === l.lastLeft && (l.lastLeft = t.scrollLeft), "top" === e && n < l.lastTop && t.dispatchEvent(o("ps-scroll-up")), "top" === e && n > l.lastTop && t.dispatchEvent(o("ps-scroll-down")), "left" === e && n < l.lastLeft && t.dispatchEvent(o("ps-scroll-left")), "left" === e && n > l.lastLeft && t.dispatchEvent(o("ps-scroll-right")), "top" === e && n !== l.lastTop && (t.scrollTop = l.lastTop = n, t.dispatchEvent(o("ps-scroll-y"))), "left" === e && n !== l.lastLeft && (t.scrollLeft = l.lastLeft = n, t.dispatchEvent(o("ps-scroll-x")));
    };
  }, {
    "./instances": 18
  }],
  21: [function (t, e, n) {
    "use strict";

    var r = t("../lib/helper"),
        o = t("../lib/dom"),
        l = t("./instances"),
        i = t("./update-geometry"),
        s = t("./update-scroll");

    e.exports = function (t) {
      var e = l.get(t);
      e && (e.negativeScrollAdjustment = e.isNegativeScroll ? t.scrollWidth - t.clientWidth : 0, o.css(e.scrollbarXRail, "display", "block"), o.css(e.scrollbarYRail, "display", "block"), e.railXMarginWidth = r.toInt(o.css(e.scrollbarXRail, "marginLeft")) + r.toInt(o.css(e.scrollbarXRail, "marginRight")), e.railYMarginHeight = r.toInt(o.css(e.scrollbarYRail, "marginTop")) + r.toInt(o.css(e.scrollbarYRail, "marginBottom")), o.css(e.scrollbarXRail, "display", "none"), o.css(e.scrollbarYRail, "display", "none"), i(t), s(t, "top", t.scrollTop), s(t, "left", t.scrollLeft), o.css(e.scrollbarXRail, "display", ""), o.css(e.scrollbarYRail, "display", ""));
    };
  }, {
    "../lib/dom": 3,
    "../lib/helper": 6,
    "./instances": 18,
    "./update-geometry": 19,
    "./update-scroll": 20
  }]
}, {}, [1]);

function copyText(text) {
  /* Get the text field */
  const el = document.createElement('textarea');
  el.value = text;
  el.setAttribute('readonly', '');
  el.style.position = 'absolute';
  el.style.left = '-9999px';
  document.body.appendChild(el);
  el.select();
  document.execCommand('copy');
  document.body.removeChild(el);
  alert("Copied the link: " + el.value);
}

function selectShippingDate() {
  const date = $("#select-delivery-date").val();

  if (typeof hours !== 'undefined' && typeof date !== 'undefined') {
    const options = hours[date].map(item => {
      return '<option value="' + item.value + '">' + item.label + '</option>';
    });
    $("#select-delivery-time").html(options);
    saveShippingDate();
  }
}

function reviewModal(id) {
  $.ajax({
    url: 'index.php?route=account/order/review_modal',
    data: {
      order_id: id
    },
    success: function (html) {
      if ($('#reviewModal').length > 0) {
        $('#reviewModal').replaceWith(html);
      } else $('body').append(html);

      $('#reviewModal').modal();
    }
  });
}

function scrollLO() {
  let a = $('#latest-order-footer .lo-wrapper'),
      b = $('#latest-order-footer .latest-order-progress'),
      target;
  if (b.hasClass('completed') === true) {
    if (b.hasClass('delivery')) target = b.find('.step-delivery .check-mark');else target = b.find('.step-4 .check-mark');
  } else if (b.hasClass('current-step-delivery') === true) target = b.find('.step-delivery .indicator');else if (b.hasClass('current-step-4') === true) target = b.find('.step-3 .check-mark');else if (b.hasClass('current-step-3') === true) target = b.find('.step-3 .check-mark');else if (b.hasClass('current-step-2') === true) target = b.find('.step-2 .check-mark');else if (b.hasClass('current-step-1') === true) target = b.find('.step-1 .check-mark');
  if (typeof target !== 'undefined') a.scrollTo(target, 800, {
    axis: 'x'
  });
}

function closeLO(id) {
  let order_close = getCookie('closedOrders');
  id = id.toString();
  if (order_close) order_close = order_close.split(',');else order_close = [];

  if (order_close.indexOf(id) == -1 || !order_close) {
    order_close.push(id);
  }

  setCookie('closedOrders', order_close.join(), 365);

  if ($('#latest-order-' + id).length > 0) {
    $('body').removeClass('latest-order-bar');
    resizeWindow();
  }
}

function saveShippingDate() {
  if ($('#select-delivery-time').val() == 'now') {
    $('#input-delivery-type').val('now');
  } else {
    $('#input-delivery-type').val('future');
  }

  var data = $('#input-delivery-type, #select-delivery-time, #select-delivery-date, #textarea-comment');
  data.push({
    name: 'delivery_time_label',
    value: $('#select-delivery-time option:selected').text()
  });
  $.ajax({
    url: 'index.php?route=checkout/shipping_datetime/save',
    type: 'post',
    data: data,
    dataType: 'json',
    success: function (json) {
      $('.alert, .text-danger').remove();

      if (json['redirect']) {
        location = json['redirect'];
      } else if (json['error']) {
        if (json['error']['warning']) {
          $('#shipping-information').prepend('<div class="alert alert-danger">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
        }
      }
    },
    error: function (xhr, ajaxOptions, thrownError) {
      alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    }
  });
}

function addClass(el, className) {
  if (el !== null) {
    if (el.classList) {
      el.classList.add(className);
    } else {
      var current = el.className,
          found = false;

      if (current) {
        var all = current.split(' ');

        for (var i = 0; i < all.length, !found; i++) found = all[i] === className;

        if (!found) {
          if (current === '') el.className = className;else el.className += ' ' + className;
        }
      }
    }
  }
}

function removeClass(el, className) {
  if (el !== null) {
    if (el.classList) el.classList.remove(className);else el.className = el.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
  }
}

function setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
  var expires = "expires=" + d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
  var name = cname + "=";
  var ca = document.cookie.split(';');

  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];

    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }

    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }

  return "";
}

function getURLVar(key) {
  var value = [];
  var query = String(document.location).split('?');

  if (query[1]) {
    var part = query[1].split('&');

    for (i = 0; i < part.length; i++) {
      var data = part[i].split('=');

      if (data[0] && data[1]) {
        value[data[0]] = data[1];
      }
    }

    if (value[key]) {
      return value[key];
    } else {
      return '';
    }
  }
}

function filterD() {
  if ($('#res_filter .selection-wrap.clone').length < 1) $('#res_filter .selection-wrap').clone().appendTo($('#res_filter .FilterSection')).addClass('clone').clone().appendTo($('#res_filter .FilterSection')).addClass('long-line').clone().appendTo($('#res_filter .FilterSection')).removeClass('long-line').addClass('break-line'); // sCVC.insertAfter($('#res_filter .selection-wrap')).addClass('long-line clone');

  $('#res_filter .selection-wrap.clone').find('input').removeAttr("id");
  var sCV = $('#res_filter	.selection-wrap:not(.clone)'),
      sCVC = $('#res_filter	.selection-wrap.clone:not(.long-line, .break-line)'),
      sCVCL = $('#res_filter	.selection-wrap.clone.long-line'),
      rC = $('#res_filter	.kitchens > ul').outerWidth(),
      rF = sCVC.outerWidth(),
      rW = $('#res_filter .container').width();

  if (rC + rF + 30 > rW) {
    $('#res_filter').addClass('break-line');

    if (rF < rW) {
      sCV.addClass('long-line');
    } else sCV.removeClass('long-line');

    if (sCVCL.outerWidth() > rW) sCV.removeClass('long-line');else sCV.addClass('long-line');

    if (sCVC.outerWidth() > rW) {
      sCV.addClass('break-line');
    } else {
      sCV.removeClass('break-line');
    }
  } else {
    $('#res_filter').removeClass('break-line');
    sCV.removeClass('long-line');
  }
}

function resizeWindow() {
  scrollLO();
  contentMinH();
  cartH();
  cateH();
  scrollStateLO();
  let wW = window.innerWidth,
      wH = window.innerHeight,
      body = $('body'),
      LOr = $('#latest-order-footer .r-wrapper'),
      LOrW = LOr.outerWidth();
  if (wW / wH > 1.7) $('body').addClass('screen-wide');else body.removeClass('screen-wide');
  filterD();
  let LOBar = $('#latest-order-footer'),
      exH = 0;
  if (wW - LOrW < 140) body.addClass('latest-order-mini');else body.removeClass('latest-order-mini');

  if (LOBar.length > 0) {
    let LBH = LOBar.outerHeight() + 5;
    let body = $('body');
    $('footer').css('padding-bottom', LBH);
    $('#fixed-nav');
    if (body.hasClass('latest-order-bar') === true && body.hasClass('cart') === false) body.find('#fixed-nav').css({
      'bottom': LBH
    });else body.find('#fixed-nav').css({
      'bottom': ''
    });
  } else {
    $('footer').css('padding-bottom', '');
    $('#fixed-nav').css('bottom', '');
  }
}

let previousScroll = 0;

function scrollWindow() {
  let headerPos = $('#header-position'),
      headerPosT = headerPos.offset().top,
      scrollT = $(window).scrollTop(),
      body = $('body'),
      header = $('header'),
      headerH = header.outerHeight();

  if (scrollT > headerPosT) {
    body.addClass('header-fixed');
    headerPos.height(headerH);
  } else {
    body.removeClass('header-fixed');
    headerPos.height(0);
    header.css({
      marginTop: 0
    });
  }

  let cmm = $('#cat-menu-mobile'),
      cmmP = $('#cat-menu-mobile-pos'),
      cmmH = cmm.outerHeight();

  if (cmm.length > 0) {
    let cmmPT = cmmP.offset().top;

    if (scrollT > cmmPT - headerH) {
      cmmP.height(cmmH);
      cmm.addClass('fixed');
      cmm.css({
        marginTop: headerH
      });
    } else {
      cmmP.height(0);
      cmm.removeClass('fixed');
      cmm.css({
        marginTop: 0
      });
    }
  }
}

jQuery.fn.extend({
  tMenu: function () {
    this.each(function () {
      var elm = $(this);
      var elm_c = elm.find('.dropdown-menu-content');
      elm_c.wrap('<div class="dropdown-menu-content-wrap"></div>');
      elm.find('.dropdown-menu-content-wrap').clone().removeClass('dropdown-menu-content-wrap').addClass('dropdown-menu-content-b').appendTo(elm);
      var elm_b = elm.find('.dropdown-menu-content-b .dropdown-menu-content');
      elm_c = elm.find('.dropdown-menu-content-wrap');
      elm.on('mouseover', function (e) {
        if ($(window).width() < 720) {} else {
          $(this).addClass('show');
          elm_c.children().width(elm_b.outerWidth());
          var mm = elm.offset().left - $('#w_w_t').offset().left + elm_b.width() - $('#w_w_t').outerWidth();
          if (mm > 0) elm_c.css('margin-left', -mm);
          elm_c.css('max-height', $(window).height() * 0.5);

          if (elm_b.height() > $(window).height() * 0.5) {
            elm.addClass('scroll');
          } else elm.removeClass('scroll');
        }
      }).on('mouseleave', function (e) {
        $(this).removeClass('show');
      });
      elm.on('click', function (e) {
        if ($(window).width() < 720) {
          if ($('#dmenu-ovl').length == 0) $('body').append('<div id="dmenu-ovl"><div class="bg"></div><div class="ct"></div></div>');else $('#dmenu-ovl .ct').html('');
          var m_ovl = $('#dmenu-ovl .ct');
          m_ovl.css({
            'min-height': $(window).height() / 2
          });
          a = elm_c.clone().appendTo(m_ovl);
          a.removeAttr('style'); //m_ovl.append('<div class="ct" style="min-height:'+$(window).height()/2+'px">'+elm_c.clone()+'</div>');

          $('body').addClass('show-dmenu-touch');
        } else if ($(this).hasClass('show') === true) $(this).trigger('mouseleave');else $(this).trigger('mouseover');

        $('#dmenu-ovl .bg').on('click', function () {
          $('body').removeClass('show-dmenu-touch');
        });
      });
    });
  },
  scrollStep: function () {
    this.each(function (i, item) {
      var elm = $(item);
      if (elm.hasClass('scroll-step-ready') === true) return false;
      elm.addClass('scroll-step-ready');
      elm.wrapInner('<div class="scroll-step-wrap" />');
      elm.append('<a class="prev" href="#"><i class="fa fa-chevron-left"></i></a><a class="next" href="#"><i class="fa fa-chevron-right"></i></a>');
      var elm_c = elm.children('.scroll-step-wrap');
      elm_c.perfectScrollbar();
      elm_c.mousewheel(function (event, delta) {
        $(this).scrollLeft -= delta * 30;
        event.preventDefault();
      });
      var clm_cc = elm_c.children('ul');
      checkWidth();

      function checkWidth() {
        if (clm_cc.outerWidth() > elm_c.parent().width()) {
          elm.addClass('scrollable');
        } else elm.removeClass('scrollable');
      }

      var i_active = elm.find('li.active');

      if (i_active.length == 1) {
        if (i_active.position().left + i_active.outerWidth() / 2 > elm.width() / 2) var i_src = i_active.position().left - elm.width() / 2 + i_active.outerWidth() / 2;else if (i_active.position().left + i_active.outerWidth() / 2 < elm.width() / 2) var i_src = 0; //elm_c.scrollLeft(i_src);

        elm_c.scrollTo(i_src, {
          axis: 'x',
          duration: 200
        });
      }

      var lCr = 0;
      var lCl = 0;
      elm_c.on('scroll', function (e) {
        var elm_c_w = this.scrollWidth;
        var target = $(e.currentTarget);
        if (target.scrollLeft() == 0) elm.addClass('f');else elm.removeClass('f');
        if (target.scrollLeft() + target.width() >= elm_c_w) elm.addClass('l');else elm.removeClass('l');
        var lC = target.find('li:last-child');
        lCr = parseInt(clm_cc.outerWidth() - lC.outerWidth() - lC.position().left);
        lCl = parseInt(target.find('li:first-child').position().left);
        target.find('li').each(function (i, element) {
          if (parseInt($(element).position().left - target.scrollLeft() + $(element).outerWidth() - target.width()) <= 0) {
            $(element).addClass('scrolled');
          } else {
            $(element).removeClass('scrolled');
          }

          if (parseInt($(element).position().left - target.scrollLeft()) < 0) $(element).addClass('over');else $(element).removeClass('over');
        });
      });
      elm_c.mouseenter(function () {
        elm_c.trigger('scroll');
      });
      elm_c.trigger('scroll');
      elm.find('.next').on('click', function (e) {
        e.preventDefault();
        var lastV = elm_c.find('li:not(.scrolled)').first();

        if (lastV.length > 0) {
          elm_c.stop();
          elm_c.scrollTo(lastV.position().left - elm.width() + lastV.outerWidth() + lCr, {
            axis: 'x',
            duration: 200
          });
        }
      });
      elm.find('.prev').on('click', function (e) {
        e.preventDefault();
        var lastV = elm_c.find('li.over').last();

        if (lastV.length > 0) {
          elm_c.stop();
          elm_c.scrollTo(lastV.position().left - lCr, {
            axis: 'x',
            duration: 200
          });
        }
      });
      $(window).resize(function () {
        checkWidth();
        elm_c.trigger('scroll');
      });
    });
  },
  fixScroll: function () {
    this.each(function () {
      var elm = $(this);
      var lastScrollTop = 0;
      var firstOffset = elm.offset().top;
      var lastA = elm.offset().top;
      var isFixed = false;
      elm.wrap('<div class="fixScroll-wrap" />');
      elm.before('<div class="target" style="width: 400px"></div>');
      var f = $(this).prev('.target');
      $(window).scroll(function (event) {
        elm.width(f.width());

        if (isFixed) {
          return;
        }

        var a = elm.offset().top;
        var b = elm.outerHeight();
        var c = $(window).outerHeight();
        var d = $(window).scrollTop();

        if (b <= c - a) {
          elm.css({
            position: "fixed"
          });
          isFixed = true;
          return;
        }

        if (d > lastScrollTop) {
          // scroll down
          if (elm.css("position") != "fixed" && c + d >= a + b) {
            elm.css({
              position: "fixed",
              bottom: 0,
              top: "auto"
            });
          }

          if (a - d >= firstOffset) {
            elm.css({
              position: "absolute",
              bottom: "auto",
              top: lastA
            });
          }
        } else {
          // scroll up
          if (a - d >= firstOffset) {
            if (elm.css("position") != "fixed") {
              elm.css({
                position: "fixed",
                bottom: "auto",
                top: firstOffset
              });
            }
          } else {
            if (elm.css("position") != "absolute") {
              elm.css({
                position: "absolute",
                bottom: "auto",
                top: lastA
              });
            }
          }
        }

        lastScrollTop = d;
        lastA = a;
      });
    });
  },
  scrollResponsive: function () {
    this.each(function () {
      var elm = $(this);

      if (elm.hasClass('scroll-ready') === false) {
        elm.wrap('<div style="overflow: auto; overflow-y:hidden; width: 100%" />');
        elm.addClass('scroll-ready');
      }
    });
  }
});

function scrollStateLO() {
  let a = $('#latest-order-footer .lo-wrapper'),
      b = a.find('.latest-order-progress'),
      aW = a.outerWidth(),
      bW = b.outerWidth(),
      aL = a.scrollLeft();

  if (aL + aW < bW) {
    $('#latest-order-footer .latest-order').addClass('scroll-not-end');
  } else $('#latest-order-footer .latest-order').removeClass('scroll-not-end');
}

function placeholderTextInput() {
  $('.text-input').each(function () {
    let a = $(this),
        placeholder = a.attr('data-placeholder');
    let value = a.text();
    value === '' && a.text(placeholder);
  });
}

$(document).ready(function () {
  placeholderTextInput();
  $('.agree-cookies').on('click', function () {
    setCookie('cb-enabled', 'accepted', 365);
    $('#cookies-bar').remove();
  });
  $('#latest-order-footer .lo-wrapper').scroll(function () {
    scrollStateLO();
  });
  $('.scroll-step').scrollStep();
  let body = $('body');
  scrollWindow();
  resizeWindow();
  $(window).resize(function () {
    resizeWindow();
    scrollWindow();
  });
  $(window).scroll(function () {
    scrollWindow();
    var currentScroll = $(this).scrollTop();

    if (currentScroll > previousScroll) {
      body.removeClass('scroll-up');
    } else {
      // scroll up
      body.addClass('scroll-up');
    }

    previousScroll = currentScroll;
  }); // Adding the clear Fix

  cols1 = $('#column-right, #column-left').length;

  if (cols1 == 2) {
    $('#content .product-layout:nth-child(2n+2)').after('<div class="clearfix visible-md visible-sm"></div>');
  } else if (cols1 == 1) {
    $('#content .product-layout:nth-child(3n+3)').after('<div class="clearfix visible-lg"></div>');
  } else {
    $('#content .product-layout:nth-child(4n+4)').after('<div class="clearfix"></div>');
  } // Highlight any found errors


  $('.text-danger').each(function () {
    var element = $(this).parent().parent();

    if (element.hasClass('form-group')) {
      element.addClass('has-error');
    }
  }); // Currency

  $('#currency .currency-select').on('click', function (e) {
    e.preventDefault();
    $('#currency input[name=\'code\']').attr('value', $(this).attr('name'));
    $('#currency').submit();
  });
  $(document).on('click', '.open-popup-cart', function (e) {
    // if($('#cart-popup').length > 0);
    $('body').addClass('popup-cart');
  });
  $(document).on('click', '.close-popup-cart', function (e) {
    $('body').removeClass('popup-cart');
  }); // Language

  $('#language a').on('click', function (e) {
    e.preventDefault();
    $('#language input[name=\'code\']').attr('value', $(this).attr('href'));
    $('#language').submit();
  }); // country

  $('#country a').on('click', function (e) {
    e.preventDefault();
    $('#country input[name=\'code\']').attr('value', $(this).attr('href'));
    $('#country').submit();
  });
  /* Search */

  $('#search input[name=\'search\']').parent().find('button').on('click', function () {
    url = $('base').attr('href') + 'index.php?route=product/search';
    var value = $('header input[name=\'search\']').val();

    if (value) {
      url += '&search=' + encodeURIComponent(value);
    }

    location = url;
  });
  $('#search input[name=\'search\']').on('keydown', function (e) {
    if (e.keyCode == 13) {
      $('header input[name=\'search\']').parent().find('button').trigger('click');
    }
  }); // Menu

  $('#menu .dropdown-menu').each(function () {
    var menu = $('#menu').offset();
    var dropdown = $(this).parent().offset();
    var i = dropdown.left + $(this).outerWidth() - (menu.left + $('#menu').outerWidth());

    if (i > 0) {
      $(this).css('margin-left', '-' + (i + 5) + 'px');
    }
  }); // Product List

  $('#list-view').click(function () {
    $('#content .product-layout > .clearfix').remove(); //$('#content .product-layout').attr('class', 'product-layout product-list col-xs-12');

    $('#content .row > .product-layout').attr('class', 'product-layout product-list col-xs-12');
    localStorage.setItem('display', 'list');
  }); // Product Grid

  $('#grid-view').click(function () {
    $('#content .product-layout > .clearfix').remove(); // What a shame bootstrap does not take into account dynamically loaded columns

    cols = $('#column-right, #column-left').length;

    if (cols == 2) {
      $('#content .product-layout').attr('class', 'product-layout product-grid col-lg-6 col-md-6 col-sm-12 col-xs-12');
    } else if (cols == 1) {
      $('#content .product-layout').attr('class', 'product-layout product-grid col-lg-4 col-md-4 col-sm-6 col-xs-12');
    } else {
      $('#content .product-layout').attr('class', 'product-layout product-grid col-lg-3 col-md-3 col-sm-6 col-xs-12');
    }

    localStorage.setItem('display', 'grid');
  });

  if (localStorage.getItem('display') == 'list') {
    $('#list-view').trigger('click');
  } else {
    $('#list-view').trigger('click');
  } // tooltips on hover


  $('[data-toggle=\'tooltip\']').tooltip({
    container: 'body'
  }); // Makes tooltips work on ajax generated content

  $(document).ajaxStop(function () {
    $('[data-toggle=\'tooltip\']').tooltip({
      container: 'body'
    });
  });
});

function cartH() {
  //cart height
  var wH = window.innerHeight;
  var e = $('#navfx2 .cart-head').outerHeight(),
      g = $('#navfx2 .nave-cation').outerHeight(),
      h = $('#navfx2 .product-list').outerHeight(),
      l = $('#navfx2 .cart-bottom').outerHeight(),
      topH = 0;

  if (window.innerWidth > 991) {
    topH = 110;
    if ($('body').hasClass('latest-order-bar')) topH = $('#latest-order-footer').outerHeight() + topH;
  }

  let k = wH - topH - e - l - g;
  $('#navfx2 .product-list').height(k);

  if ($('#navfx2').outerHeight() > k) {
    $('#navfx2 .product-list').addClass('scrollable');
  } else $('#navfx2 .product-list').removeClass('scrollable');
}

function contentMinH() {
  var wH = window.innerHeight,
      hH = $('header').outerHeight(),
      fH = $('footer').outerHeight();
  $('#container').css({
    'min-height': wH - hH - fH + 'px'
  });
}

function cateH() {
  let wH = window.innerHeight,
      e = $('header').outerHeight(),
      f = wH - e,
      g = $('#navfx').outerHeight();
  $('.sidebar-menu').height(f);
  if (g > f) $('.sidebar-menu').addClass('scrollable');else $('.sidebar-menu').removeClass('scrollable');
}

function notification(msg, type, name, style, time) {
  if (!type) type = 'top';
  if (!name) name = 'default';
  let a = $('#notification-' + type),
      b = $('<div id="notification-' + type + '" class="notification"></div>'),
      c = $('<div class="' + style + '" id="ntf-' + type + '-' + name + '"><div class="msg">' + msg + '</div><button class="close"><span aria-hidden="true">&times</span></button></div>');
  if (a.length == 0) $('body').append(b);else b = a;
  if ($('#ntf-' + type + '-' + name).length == 0) b.append(c);else {
    $('#ntf-' + type + '-' + name + ' .msg').html(msg);
    $('#ntf-' + type + '-' + name).attr('class', style);
  }
  if (typeof time === 'undefined') time = 0;else time = parseInt(time);
  if (time) setTimeout(function () {
    $('#ntf-' + type + '-' + name).find('button.close').trigger('click');
  }, time);
}

function updatePrdCart(data) {
  var a = $('#restaurant-cart .product-list > .quantity-adder-name[data-product-key="' + data['key'] + '"]'),
      options = '',
      data_option = data['option_data'];

  if (typeof data_option != 'undefined') {
    Object.keys(data_option).forEach(function (key) {
      options += '<p>';
      options += '<span>' + key + ':</span> <span>';
      options += data_option[key].join(', </span>' + '<span>');
      options += '</span></p>';
    });
  }

  ;
  if (data['total'] && !data['total_formatted']) data['total_formatted'] = numeral(data['total']).format(0, 0) + ' ' + currency;
  if (data['price'] && !data['price_formatted']) data['price_formatted'] = numeral(data['price']).format(0, 0) + ' ' + currency;

  if (a.length < 1) {
    let html = '';
    html = '<div class="quantity-adder-name product-' + data['product_id'] + '"' + ' data-product-key="' + data['key'] + '"' + ' data-product-id="' + data['product_id'] + '"' + ' data-store-id="' + data['vendor_id'] + '"' + ' data-price="' + data['price'] + '"' + ' data-price-total="' + data['total'] + '"' + ' data-qty="' + data['quantity'] + '"' + ' data-packaging-fee="' + data['packaging_fee'] + '"' + ' data-packaging-fee-total="' + data['packaging_fee_total'] + '"' + '>';
    html += '<div class="btn-l">' + '<button type="button" class="delete" onclick="cart.remove(\'' + data['key'] + '\');">' + '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 298.667 298.667" style="enable-background:new 0 0 298.667 298.667;" xml:space="preserve"><g><g><polygon points="298.667,30.187 268.48,0 149.333,119.147 30.187,0 0,30.187 119.147,149.333 0,268.48 30.187,298.667 149.333,179.52 268.48,298.667 298.667,268.48 179.52,149.333 "></polygon></g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg>' + '</button>' + '</div>';
    html += '<div class="cart-branch-name"><span>' + data['product_name'] + '</span></div>';
    html += '<div class="meal-extra-name">';

    if (options) {
      html += options;
    }

    html += '</div>';
    html += '<div class="note">' + '<div class="note-quote"><span>';
    if (data['special_request']) html += '** ' + data['special_request'];
    html += '</span></div>' + '<div class="note-editor">' + '<span class="text-input" contenteditable="true" data-url="/index.php?route=checkout/checkout/addSpecialRequest">' + data['special_request'] + '</span> ' + '</div>' + '</div>';
    html += '<div class="r">';
    html += '<div class="qty-incres"><span><span class="qty-incres-n">' + data['quantity'] + '</span> ×</span> <span class="price"><span class="n">' + data['price_formatted'] + '</span></span></div>';
    html += '<div class="qty-number">' + '<button data-key="' + data['key'] + '" data-step="1" data-old-quantity="' + data['quantity'] + '" data-current-quantity="' + data['quantity'] + '" type="button" class="btn btn-secondary add-down add-action"><i class="fas fa-minus"></i></button>' + '<button data-key="' + data['key'] + '" data-step="1" data-old-quantity="' + data['quantity'] + '" data-current-quantity="' + data['quantity'] + '" type="button" class="btn btn-secondary add-up add-action"><i class="fas fa-plus"></i></button>';
    html += '<div class="cart-price-list"><span class="n">' + data['total_formatted'] + '</span></div>';
    html += '</div>';
    html += '<div class="cart-price-deleted">' + '<button class="message" type="button"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 477.867 477.867" style="enable-background:new 0 0 477.867 477.867;" xml:space="preserve"><g><g><path d="M426.667,0.002H51.2C22.923,0.002,0,22.925,0,51.202v273.067c0,28.277,22.923,51.2,51.2,51.2h60.587l-9.284,83.456 c-1.035,9.369,5.721,17.802,15.09,18.837c4.838,0.534,9.674-1.023,13.292-4.279l108.919-98.014h186.863 c28.277,0,51.2-22.923,51.2-51.2V51.202C477.867,22.925,454.944,0.002,426.667,0.002z M443.733,324.269 c0,9.426-7.641,17.067-17.067,17.067H233.25c-4.217,0.001-8.284,1.564-11.418,4.386l-80.452,72.414l6.434-57.839 c1.046-9.367-5.699-17.809-15.067-18.856c-0.63-0.07-1.263-0.106-1.897-0.105H51.2c-9.426,0-17.067-7.641-17.067-17.067V51.202 c0-9.426,7.641-17.067,17.067-17.067h375.467c9.426,0,17.067,7.641,17.067,17.067V324.269z"></path></g> </g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg>' + '</button>' + '</div>';
    html += '</div>';
    $('#restaurant-cart .product-list').prepend(html);
  } else {
    let aPrice = a.attr('data-price'),
        aPriceT = a.attr('data-price-total'),
        aQty = a.attr('data-qty');
    a.attr('data-qty', data['quantity']);
    a.attr('data-price-total', data['total']);
    if (!data['packaging_fee']) data['packaging_fee'] = parseFloat(a.attr('data-packaging-fee'));
    a.attr('data-packaging-fee-total', data['packaging-fee-total']);
    a.attr('data-packaging-fee', data['packaging_fee']);
    a.attr('data-price', data['price']);
    a.find('.qty-incres-n').html(data['quantity']);

    if (typeof data['special_request'] !== 'undefined') {
      if (data['special_request'] != '') a.find('.note-quote span').html('** ' + data['special_request']);else a.find('.note-quote span').html('');
      a.find('.note-editor span').text(data['special_request']);
    }

    a.find('.price .n').html(data['price_formatted']); // $('.package-total-n').html(data['packaging_fee_total_formatted']);

    a.find('.cart-price-list .n').html(data['total_formatted']);
    a.find('button.add-up, button.add-down').attr("data-old-quantity", data['quantity']);
    a.find('button.add-up, button.add-down').attr("data-current-quantity", data['quantity']); // a.find('button.add-down').attr('onclick', 'cart.update(\''+data['key']+'\', \''+(data['quantity']-1)+'\')');

    if (options) a.find('.meal-extra-name').html(options);
  }

  if (data['packaging_fee_cart_calc']) {
    let calc = '';
    data['packaging_fee_cart_calc'].forEach(function (item, index) {
      calc += '<span>';
      if (index > 0) calc += ' + ';
      calc += item + '</span>' + '';
    });
    $('#restaurant-cart .packaging-fee .qty-incres > span').html(calc);
  }

  if (typeof data['cart_totals'] !== 'undefined') {
    if (typeof data['cart_totals']['coupon'] !== 'undefined') {
      let coupon = data['cart_totals']['coupon'];
      $('#restaurant-cart').attr('data-cost-coupon', coupon['value']);
    }

    if (typeof data['cart_totals']['total'] !== 'undefined') {
      $('#restaurant-cart').attr('data-cost-total', data['cart_totals']['total']['value']);
    }
  }

  $('#restaurant-cart').attr('data-quantity-total', data['quantity_total']);
  if (data['vendor_id']) $('#restaurant-cart').attr('data-cart-store', data['vendor_id']);
  let productID = a.attr('data-product-id'),
      store = a.attr('data-store-id');
  if (!productID) productID = data['product_id'];
  if (!store) store = data['vendor_id'];
  checkQty(productID, store);
}

function viewQty(product, store, num) {
  let html = '<div class="menu-list-item"> <i class="fas fa-check" aria-hidden="true"></i>' + // '<span class="in-cart">V košíku</span>' +
  '<span class="in-cart-qty"><span class="c' + product + '-' + store + '" >' + num + '</span></span></div>';
  /* let spanvalue = parseInt($('#c'+product+'-'+store).text());
  if(spanvalue)
  {
  	$('#c'+product+'-'+store).text(prdC);
  }else{
  	$('#p'+product+'-'+store).html(html);
  }*/

  $('.p' + product + '-' + store).html(html);
  if (num == 0) $('.p' + product + '-' + store).html('');
}

function checkQty(product, store) {
  let prdC = 0;
  $('#restaurant-cart').find('.product-' + product).each(function () {
    let qty = parseFloat($(this).attr('data-qty'));
    if (!isNaN(qty)) prdC = prdC + qty;
  });
  viewQty(product, store, prdC);
}

let cart = {
  'add': function (product_id, quantity, vendor_id, product_to_menu_id) {
    $.ajax({
      url: 'index.php?route=checkout/cart/add',
      type: 'post',
      data: 'product_id=' + product_id + '&quantity=' + (typeof quantity != 'undefined' ? quantity : 1) + '&vendor_id=' + vendor_id + '&product_to_menu_id=' + product_to_menu_id,
      dataType: 'json',
      beforeSend: function () {
        $('#cart > button').button('loading');
      },
      complete: function () {
        $('#cart > button').button('reset');
      },
      success: function (json) {
        $('.alert, .text-danger').remove();
        cart.info();

        if (json['error']) {
          if (json['error']['stock'] || json['error']['branch_status']) {
            if (json['error']['stock']) {
              $('#restaurant-alert-status').find('.modal-body p').html(json['error']['stock']);
            }

            if (json['error']['branch_status']) {
              $('#restaurant-alert-status').find('.modal-body p').html(json['error']['branch_status']);
            }

            $('#restaurant-alert-status').modal('show');
          }
        }

        if (json['success']) {// $.ajax({
          // 	url: 'index.php?route=restaurant/cart',
          // 	cache:false,
          // 	success:function (data) {
          // 		// $('#restaurant-cart').html(data);
          // 		// cartH();
          //
          // 	}
          // })
          // $('#restaurant-cart-footer').load('index.php?route=common/cart/footerinfo');
        }
      }
    });
  },
  'update': function (key, quantity) {
    if (typeof editByKeyRequest !== "undefined") editByKeyRequest.abort();
    editByKeyRequest = $.ajax({
      url: 'index.php?route=checkout/cart/editByKey',
      type: 'post',
      data: 'key=' + key + '&quantity=' + (typeof quantity != 'undefined' ? quantity : 1),
      dataType: 'json',
      beforeSend: function () {
        $('#cart > button').button('loading');
      },
      complete: function () {
        $('#cart > button').button('reset');
      },
      success: function (json) {
        // $.ajax({
        // 	url: 'index.php?route=restaurant/cart',
        // 	cache:false,
        // 	success:function (data) {
        // 		$('#restaurant-cart').html(data);
        // 		cartH();
        // 	}
        // })
        if (json['success']) {
          updatePrdCart(json);
          cart.info();
        } // $('#restaurant-cart').load('index.php?route=restaurant/cart');
        // $('#restaurant-cart-footer').load('index.php?route=common/cart/footerinfo');
        // location.reload();

      }
    });
  },
  'ordertype': function (order_type) {
    if (typeof orderTypeRequest !== "undefined") orderTypeRequest.abort();
    orderTypeRequest = $.ajax({
      url: 'index.php?route=checkout/cart/setOrdertype&order_type=' + order_type,
      type: 'post',
      data: 'key=' + order_type,
      dataType: 'json',
      beforeSend: function () {
        $('#cart > button').button('loading');
      },
      complete: function () {
        $('#cart > button').button('reset');
      },
      success: function (json) {
        $('#input-order-type').val(order_type);
        $('button.ordertype-button:not(.ordertype-' + order_type + ')').removeClass('active');
        $('button.ordertype-' + order_type).addClass('active');
        $('#restaurant-cart').attr('data-delivery-type', order_type);
        cart.info();
        setCookie('deliveryType', order_type, 365);
        let $el = $("#select-delivery-date");
        $el.empty(); // remove old options

        $.each(json['dates'], function (key, value) {
          $el.append($("<option></option>").attr("value", value.value).text(value.label));
        });
        hours = json['hours'];
        selectShippingDate();
        $('.text_delivery_time').html(json.text_time);
        $('.text_delivery_date').html(json.text_date);
      }
    });
  },
  'remove': function (key) {
    $.ajax({
      url: 'index.php?route=checkout/cart/remove',
      type: 'post',
      data: 'key=' + key,
      dataType: 'json',
      beforeSend: function () {
        $('#cart > button').button('loading');
      },
      complete: function () {
        $('#cart > button').button('reset');
      },
      success: function (json) {
        updatePrdCart(json);
        $('#restaurant-cart .quantity-adder-name[data-product-key="' + key + '"]').remove();
        checkQty(json['product_id'], json['vendor_id']);
        cart.info(); // $.ajax({
        // 	url: 'index.php?route=common/cart/footerinfo',
        // 	cache:false,
        // 	success:function (data) {
        // 		$('#restaurant-cart-footer').html(data);
        // 		if($('#cart-popup').length < 1) $('body').removeClass('popup-cart')
        // 	}
        // })
        // location.reload();
      }
    });
  },
  'info': function () {
    let c = document.getElementById("restaurant-cart"),
        total = 0,
        subTotal = 0,
        packageFee = 0,
        count = 0,
        diff2min = 0,
        d = document.querySelectorAll('#restaurant-cart .quantity-adder-name:not(.packaging-fee)'),
        minimumAmount = parseFloat(c.getAttribute('data-minimum-amount')),
        quantityTotal = 0,
        deliveryType = c.getAttribute('data-delivery-type'),
        deliveryFee = parseFloat(c.getAttribute('data-shipping')),
        coupon = parseFloat(c.getAttribute('data-cost-coupon')),
        cf = document.getElementById("checkout-confirm");
    if (isNaN(coupon)) coupon = 0;
    if (isNaN(deliveryFee)) deliveryFee = 0;
    Array.prototype.forEach.call(d, function (el, i) {
      let p = parseFloat(el.getAttribute('data-price-total'));
      let pf = parseFloat(el.getAttribute('data-packaging-fee-total'));
      quantityTotal = quantityTotal + parseFloat(el.getAttribute('data-qty'));
      if (isNaN(p)) p = 0;
      if (isNaN(pf)) pf = 0;
      subTotal += p;
      packageFee += pf;
    });

    if (deliveryType == 'pickup') {
      addClass(cf, 'pickup');
      removeClass(cf, 'delivery');
    } else {
      removeClass(cf, 'pickup');
      addClass(cf, 'delivery');
    }

    subTotal = subTotal + packageFee;

    if (c) {
      c.setAttribute('data-cost-sub_total', subTotal);
      c.setAttribute('data-quantity-total', quantityTotal);
    }

    [].forEach.call(document.querySelectorAll('#restaurant-cart-footer-bar .cart-quantity-n, #cart-total .itemcount'), function (i) {
      i.innerHTML = quantityTotal;
    });

    if (c) {
      if (minimumAmount > subTotal && deliveryType == 'delivery') {
        diff2min = minimumAmount - subTotal;
        total = minimumAmount + deliveryFee + coupon;
        removeClass(c, 'over-min');
      } else {
        let ma = document.querySelectorAll('#mini-cart-total .minimum-amount');

        if (deliveryType == 'delivery') {
          total = subTotal + deliveryFee + coupon;
          addClass(c, 'over-min');
        } else total = subTotal + coupon;
      }

      let e = c.querySelectorAll('.diff-minimum-amount-n');
      if (e.length > 0) e[0].innerHTML = numeral(diff2min).format(0, 0) + ' ' + currency;
      const costV = c.getAttribute('data-cost-coupon');
      if (!costV) c.setAttribute('data-cost-total', total);
    }

    var nodes = [],
        values = [];
    var re = /^data-cost-/;

    for (var att, i = 0, atts = c.attributes, n = atts.length; i < n; i++) {
      att = atts[i];

      if (att.nodeName.match(re)) {
        // nodes.push(att.nodeName);
        // values.push(att.nodeValue);
        let ttis = document.querySelectorAll('.total-' + att.nodeName.replace('data-cost-', '') + '-n');
        let attV = att.nodeValue;

        if (!isNaN(attV)) {
          attV = numeral(att.nodeValue).format(0, 0) + ' ' + currency;
        }

        ttis.innerHTML = attV;
        [].forEach.call(ttis, function (tti) {
          tti.innerHTML = attV;
        });
      }
    }

    if (d.length == 0) {
      // let e = c.querySelectorAll('.quantity-adder-name.packaging-fee');
      // e[0].parentNode.removeChild(e[0]);
      addClass(c, 'empty');
      removeClass(document.querySelectorAll('body')[0], 'cart');
      addClass(document.getElementById("restaurant-cart-footer-bar"), 'empty');
      [].forEach.call(document.querySelectorAll('.total-sub_total-n, .total-total-n, .total-coupon-n'), function (i) {
        i.innerHTML = '-';
      });
      let e = document.querySelectorAll('.cart-quantity-n');
      if (e.length != 0) e[0].innerHTML = '0';
    } else {
      addClass(document.querySelectorAll('body')[0], 'cart');
      removeClass(c, 'empty');
      removeClass(document.getElementById("restaurant-cart-footer-bar"), 'empty');
    }

    cartH();
  },
  'clear': function () {
    $.ajax({
      url: 'index.php?route=checkout/cart/clear',
      type: 'get',
      beforeSend: function () {
        $('#button-clear-cart').button('loading');
      },
      complete: function () {
        $('#button-clear-cart').button('reset');
      },
      success: function (json) {
        $('#cart-alert-status').modal('hide');
        if ($('#restaurant-cart').length > 0) cart.load();
        $('.menu-list-item, #top-notify').remove();
        $('#restaurant-cart-footer-bar').addClass('empty');
        storeInCart = 0;
        if (add2Cart) submitCart(add2Cart);
      }
    });
  },
  'load': function () {
    $.ajax({
      url: 'index.php?route=restaurant/cart',
      data: {
        storeID: storeID
      },
      cache: false,
      success: function (data) {
        $('#restaurant-cart-wrap').html(data);
        cart.info();
        cartH();
      }
    });
  }
};
let voucher = {
  'add': function () {},
  'remove': function (key) {
    $.ajax({
      url: 'index.php?route=checkout/cart/remove',
      type: 'post',
      data: 'key=' + key,
      dataType: 'json',
      beforeSend: function () {
        $('#cart > button').button('loading');
      },
      complete: function () {
        $('#cart > button').button('reset');
      },
      success: function (json) {
        // Need to set timeout otherwise it wont update the total
        setTimeout(function () {
          $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
        }, 100);

        if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
          location = 'index.php?route=checkout/cart';
        } else {
          $.ajax({
            url: 'index.php?route=common/cart/info ul li',
            cache: false,
            success: function (data) {
              $('#cart > ul').html(data);
              cartH();
            }
          });
        }
      }
    });
  }
};
let wishlist = {
  'add': function (product_id) {
    $.ajax({
      url: 'index.php?route=account/wishlist/add',
      type: 'post',
      data: 'product_id=' + product_id,
      dataType: 'json',
      success: function (json) {
        $('.alert').remove();

        if (json['success']) {
          $('#content').parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
        }

        if (json['info']) {
          $('#content').parent().before('<div class="alert alert-info"><i class="fa fa-info-circle"></i> ' + json['info'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
        }

        $('#wishlist-total').html(json['total']);
        $('html, body').animate({
          scrollTop: 0
        }, 'slow');
      }
    });
  },
  'remove': function () {}
};
let compare = {
  'add': function (product_id) {
    $.ajax({
      url: 'index.php?route=product/compare/add',
      type: 'post',
      data: 'product_id=' + product_id,
      dataType: 'json',
      success: function (json) {
        $('.alert').remove();

        if (json['success']) {
          $('#content').parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
          $('#compare-total').html(json['total']);
          $('html, body').animate({
            scrollTop: 0
          }, 'slow');
        }
      }
    });
  },
  'remove': function () {}
};
/* Agree to Terms */

$(document).delegate('.agree', 'click', function (e) {
  e.preventDefault();
  $('#modal-agree').remove();
  var element = this;
  $.ajax({
    url: $(element).attr('href'),
    type: 'get',
    dataType: 'html',
    success: function (data) {
      html = '<div id="modal-agree" class="modal">';
      html += '  <div class="modal-dialog">';
      html += '    <div class="modal-content">';
      html += '      <div class="modal-header">';
      html += '        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
      html += '        <h4 class="modal-title">' + $(element).text() + '</h4>';
      html += '      </div>';
      html += '      <div class="modal-body">' + data + '</div>';
      html += '    </div';
      html += '  </div>';
      html += '</div>';
      $('body').append(html);
      $('#modal-agree').modal('show');
    }
  });
});
$(document).delegate('.popup > section .close-popup', 'click', function (e) {
  let a = $(this),
      b = a.parents('section');
  closePopup();
});

function closePopup() {
  $('body').removeClass('open-popup');
} // function popup(url, type, id, loader_inner, cache)


function popup(arg) {
  let loader,
      b,
      type,
      d = $('.popup'),
      b_id = 0;
  if (typeof arg.loader != 'undefined' || !type) type = arg.type;else type = 'popup';
  if (typeof arg.id != 'undefined') b_id = arg.id;
  let f = $('section#popup-' + type + '-' + b_id);
  $('.popup section').removeClass('opened');

  function action() {
    if (f.length < 1) {
      if (typeof arg.loader != 'undefined') {
        b = arg.loader;
        b.addClass('loading');
        loader = $('<div class="loader"><div class="loader-wrapper">' + loadingSpinner + '</div></div>'), l = b.find('.loader');

        if (l.length > 0) {
          l.html('<div class="loader-wrapper">' + loadingSpinner + '</div>');
        } else b.append(loader);
      }

      function error() {
        let button = $('<button class="btn-default">Thử lại</button>'),
            close = $('<button class="btn-default">Hủy</button>'),
            lW,
            msg = $('<p><i class="fas fa-exclamation-triangle"></i> Lỗi!</p>'),
            btns = $('<p></p>');
        if (b) lW = b.find('.loader-wrapper'), btns.append(close).append(' ').append(button);
        if (lW) lW.html(msg).append(btns);
        button.click(function () {
          action();
        });
        close.click(function () {
          b.removeClass('loading');
          loader.remove();
          b.removeClass('order-loaded');
        });
      }

      function req() {
        $.ajax({
          url: arg.url,
          success: function (html) {
            if (html) {
              // b.append('<div class="order-expanded">'+html+'</div>');
              // b.addClass('order-loaded');
              if (b) b.removeClass('loading');
              if (loader) loader.remove(); // $.scrollTo(a.find('.order-expanded'));
              // window.history.pushState("object or string", "Title", a.attr('href'));

              if (d.length < 1) {
                d = $('<div id="popup" class="popup"></div>');
                $('body').append(d);
              }

              if (f.length < 1) d.append('<section class="' + type + ' opened" id="popup-' + type + '-' + b_id + '">' + html + '</section>');else f.replaceWith(html);
              $('body').addClass('open-popup open-popup-' + type);
              d.scrollTop(0);
            } else error();
          },
          error: function () {
            error();
          }
        });
      }

      req();
    } else {
      $('body').addClass('open-popup open-popup-' + type);
      f.addClass('opened');
      d.scrollTop(0);

      if (type == 'product' && typeof arg.data != 'undefined') {
        let product_title = product_opened.get(b_id).product_title;

        if (typeof arg.data.product_path != 'undefined') {
          history.pushState({}, product_title, arg.data.product_path);
          document.title = product_title;
        }
      }
    }
  }

  return action();
}

$(document).delegate('section.order a.load-order', 'click', function (e) {
  e.preventDefault();
  let a = $(this),
      b = a.parents('section'),
      c = b.find('.order-expanded'),
      d = $('#popup'),
      b_id = b.attr('id').replace('order-', ''),
      f = d.find('section#popup-order-' + b_id),
      url = a.attr('href');
  popup({
    url: url,
    loader: b,
    type: 'order',
    id: b_id
  });
});
var requestSent = false;
$(document).delegate('#popup', 'click', function (event) {
  let e = $(event.target),
      a = e.attr('id');

  if (a == 'popup') {
    let p = e.find('section.product.opened button.close');
    if (p.length > 0) p.trigger('click');
    closePopup();
  }
});
$(document).delegate('#popup section.product .product-image', 'click', function (event) {
  let e = $(event.target),
      a = e.parents('section.product');
  let p = a.find('button.close');
  if (p.length > 0) p.trigger('click');
  closePopup();
});
$(document).delegate('.notification button.close', 'click', function () {
  let a = $(this),
      b = a.parents('.notification');
  a.parent().remove();
  if (b.children('div').length < 1) b.remove();
});
$(document).delegate('#button-place-order', 'click', function () {
  if (!requestSent) {
    requestSent = true;
    $.ajax({
      url: 'index.php?route=checkout/confirm',
      type: 'post',
      data: $('#collapse-checkout-method input[type=\'radio\']:checked, #collapse-checkout-method textarea, #collapse-pickup-type input[type=\'text\'], #collapse-delivery-type input[type=\'text\']'),
      dataType: 'json',
      beforeSend: function () {
        $('#button-place-order').button('loading');
        $("body").addClass("loading");
      },
      complete: function () {
        requestSent = false;
      },
      success: function (json) {
        $('.alert, .text-danger').remove();

        if (json['redirect']) {
          location = json['redirect'];
        } else if (json['error']) {
          if (json['error']['warning']) {
            $('#restaurant-alert-status').modal('show');
            $('#restaurant-alert-status').find('.modal-body p').html(json['error']['warning']);
            $('#button-place-order').button('reset');
            $("body").removeClass("loading");
          }
        } else {
          if (json['payment']) {
            $('#button-place-order').after(json['payment']);
            $('form#form-trigger-payment').submit();
          }
        }
      },
      error: function (xhr, ajaxOptions, thrownError) {}
    });
  }
});
$(document).delegate('.addToCart', 'click', function () {
  let addcartId = parseInt($(this).attr('data-cart-id'));
  $.ajax({
    url: 'index.php?route=checkout/cart/addToCart',
    type: 'post',
    data: 'cartID=' + addcartId,
    beforeSend: function () {},
    complete: function () {},
    success: function (json) {
      // console.log(json)
      $('.alert, .text-danger').remove();
      $('.form-group').removeClass('has-error');

      if (json['error']) {
        if (json['error']['verify']) {
          $('#cart-alert-status').find('.modal-body p').html(json['error']['verify']);
          $('#cart-alert-status').find('.modal-footer').html('<button type="button" id="button-clear-cart" class="button-clear-cart btn btn-lg btn-success" data-dismiss="modal">Yes</button><button type="button" class="btn btn-lg btn-warning" data-dismiss="modal">No</button>');
          $('#cart-alert-status').modal('show');
        } else {
          if (json['error']['stock'] || json['error']['branch_status']) {
            if (json['error']['stock']) {
              $('#restaurant-alert-status').find('.modal-body p').html(json['error']['stock']);
            }

            if (json['error']['branch_status']) {
              $('#restaurant-alert-status').find('.modal-body p').html(json['error']['branch_status']);
            }

            $('#restaurant-alert-status').modal('show');
          }
        }
      }

      if (json['options'] && json['options'].length > 0) {
        $('#productOptions').modal('show');
        html = '<div class="modal-header">';
        html += '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
        html += '<h3 class="modal-title"><?php echo $text_option; ?></h3>';
        html += ' </div>';
        html += '<div class="col-lg-12 col-sm-12 col-xs-12 modal-body">';
        html += '<div class="hidden form-group required">';
        html += '<input type="hidden" name="product_id" value="' + json['product_id'] + '" >'; // html += '<input type="hidden" name="product_to_menu_id" value="'+json['product_to_menu_id']+'" >';

        html += '<input type="hidden" name="vendor_id" value="' + json['vendor_id'] + '" >';
        html += ' </div>';
        html += '<div class="col-lg-6 col-sm-6 col-xs-12 choices-product">';
        html += '<b>' + json['product_name'] + '</b>';
        html += '<p>' + json['product_desc'] + '</p>';
        html += '</div>';
        html += '<div class="col-lg-3 col-sm-3 col-xs-12 popup-qty-popup form-group required">';
        html += '<span><b><?php echo $text_quantity;?></b></span>';
        html += '<button type="button" class="hidden add-up add-action"><i class="fas fa-minus"></i></button>';
        html += '<input type="text" maxlength="2" name="quantity" value="' + json['minimum'] + '" size="1" id="input-quantity" class="form-control" />';
        html += '<button type="button" class="hidden add-up add-action"><i class="fas fa-plus"></i></button>';
        html += '</div>';
        html += '<div class="col-lg-3 col-sm-3 col-xs-12 popup-qty-price pull-right form-group required">';

        if (json['price']) {
          html += '<div class="pull-right">';
          html += '<span><b><?php echo $text_price;?> : </b></span>';
          html += '<span class="red-price"><b>' + json['price'] + '</b></span>';
          html += '</div>';
        }

        html += '</div>';

        for (i = 0; i < json['options'].length; i++) {
          if (json['options'][i]['type'] == 'radio') {
            if (json['options'][i]['required'] > 0) {
              var required = ' required';
              var choose = ' (Choose 1)';
            } else {
              var required = '';
              var choose = '';
            }

            html += '<div class="menu-head">';
            html += '<div class="form-group' + required + '">';
            html += '<label class="control-label">' + json['options'][i]['name'] + ' ' + choose + '</label>';
            html += '<div id="input-option' + json['options'][i]['product_option_id'] + '">';

            for (j = 0; j < json['options'][i]['product_option_value'].length; j++) {
              html += '<div class="radio">';
              html += '<label>';
              html += '<input type="radio" name="option[' + json['options'][i]['product_option_id'] + ']" value="' + json['options'][i]['product_option_value'][j]['product_option_value_id'] + '">';
              html += '<span class="checkmark"></span>';
              html += '<span class="name"><span class="middle-aligner"></span>' + json['options'][i]['product_option_value'][j]['name'] + '</span>';

              if (json['options'][i]['product_option_value'][j]['price']) {
                html += ' (<span class="price">+' + json['options'][i]['product_option_value'][j]['price'] + '</span>)';
              }

              html += '</label>';
              html += '</div>';
            }

            html += ' </div>';
            html += ' </div>';
            html += '</div>';
          }

          if (json['options'][i]['type'] == 'checkbox') {
            if (json['options'][i]['required'] > 0) {
              var required = ' required';

              if (json['options'][i]['minimum_limit'] == 0 && json['options'][i]['maximum_limit'] == 0) {
                var choose = '';
              } else if (json['options'][i]['minimum_limit'] > 0 && json['options'][i]['maximum_limit'] == 0) {
                var choose = '( Minimum = ' + json['options'][i]['minimum_limit'] + ')';
              } else if (json['options'][i]['minimum_limit'] == 0 && json['options'][i]['maximum_limit'] > 0) {
                var choose = '( Maximum = ' + json['options'][i]['maximum_limit'] + ')';
              } else {
                var choose = '( Minimum = ' + json['options'][i]['minimum_limit'] + ', Maximum = ' + json['options'][i]['maximum_limit'] + ')';
              }
            } else {
              var required = '';
              var choose = '';
            }

            html += '<div class="form-group' + required + '">';
            html += '<label class="control-label">' + json['options'][i]['name'] + ' ' + choose + '</label>';
            html += '<div id="input-option' + json['options'][i]['product_option_id'] + '">';

            for (k = 0; k < json['options'][i]['product_option_value'].length; k++) {
              html += '<div class="checkbox">';
              html += '<label>';
              html += '<input type="checkbox" name="option[' + json['options'][i]['product_option_id'] + '][]" value="' + json['options'][i]['product_option_value'][k]['product_option_value_id'] + '">';
              html += '<span class="checkmark"></span>';
              html += '<span class="name"><span class="middle-aligner"></span>' + json['options'][i]['product_option_value'][k]['name'] + '</span>';

              if (json['options'][i]['product_option_value'][k]['price']) {
                html += ' (<span class="price">+' + json['options'][i]['product_option_value'][k]['price'] + '</span>)';
              }

              html += '</label>';
              html += '</div>';
            }

            html += ' </div>';
            html += ' </div>';
          }
        }

        html += '</div>';
        html += '<div class="col-lg-12 col-sm-12 col-xs-12 modal-footer">';
        html += '<button id="button-cart-' + addcartId + '" type="button" class="btn btn-primary button-add-cart"><i class="fas fa-shopping-cart" aria-hidden="true"></i>Add to Cart</button>';
        html += '</div>';
        $('#productOptions').find('.modal-content').html(html);
      }

      if (json['success']) {
        if (typeof storeID === "undefined") {
          storeInCart = json['store_href'];
          window.location = storeInCart;
          return false;
        }

        if ($('#restaurant-cart .quantity-adder-name:not(.packaging-fee)').length < 1) {
          $.ajax({
            url: 'index.php?route=restaurant/cart',
            data: {
              storeID: storeID
            },
            cache: false,
            success: function (data) {
              $('#restaurant-cart-wrap').html(data);
              cart.info();
              checkQty(json['product_id'], json['vendor_id']);
              cartH();
            }
          });
        } else {
          updatePrdCart(json);
          cart.info();
        } // swal({
        // 	type: "success",
        //          	title: "Success",
        //          	text: 'You have added <span style="color:#EA5B31;font-weight: bold">'+json['product_name']+' </span> to your cart!',
        //          	timer: 2000,
        //          	html: true
        //          	// showConfirmButton: false
        //         	});
        // var scrollBottom = $(window).scrollTop() + window.innerHeight;
        //$('html, body').animate({ scrollTop: scrollBottom }, 'slow');
        // $.ajax({
        // 	url: 'index.php?route=restaurant/cart',
        // 	cache:false,
        // 	success:function (data) {
        // 		$('#restaurant-cart').html(data);
        // 		cartH();
        // 	}
        // })
        // $('#restaurant-cart-footer').load('index.php?route=common/cart/footerinfo');

      }
    }
  });
});

function openOptions(id) {
  $('.featured-top-product .title-overlay').removeClass('show-options');
  $('.title-overlay#' + id).addClass('show-options');
}

function closeOptions(id) {
  $('.title-overlay#' + id).removeClass('show-options');
}

$(document).delegate('form.add2cart', 'submit', function (event) {
  let a = $(event.target),
      form_c = a.serializeArray(),
      valid = true,
      required = a.find('input.required'),
      radioN = [];
  event.preventDefault(); // checkbox.each(function () {
  // 	if($(this).prop('checked') === false) valid = false;
  // });

  required.each(function () {
    let name = $(this).attr('name');

    if (radioN.indexOf(name) == -1 || !radioN) {
      radioN.push(name);
    }
  });
  let checked = 0;

  for (let i = 0; i < radioN.length; i++) {
    let ip = a.find("input[name='" + radioN[i] + "']"),
        checked_i = 0;
    ip.each(function () {
      if ($(this).prop('checked') === true) {
        checked_i = 1;
      }
    });

    if (checked_i == 0) {// let a = $('<div class="error"></div>')
      // ip.first().parentsUntil('form-group').find('label.control-label').after('11')
      // ip.first().prevUntil( "label" ).append('111')
    }

    checked = checked + checked_i;
  }

  if (checked < radioN.length) {
    valid = false;
  }

  if (valid) {
    if (!latitude || !longitude) {
      add2Cart = form_c;
      openAddressPopup();
      return false;
    }

    submitCart(form_c);
  } else {
    let b = a.parents('.title-overlay');

    if (b.hasClass('show-options') === false) {
      b.addClass('show-options');
    } else {
      $('#cart-alert-status').find('.modal-body p').html('<span class="required">*</span> je povinné');
      $('#cart-alert-status').find('.modal-footer').html('<button type="button" class="btn btn-lg btn-warning" data-dismiss="modal">OK</button>');
      $('#cart-alert-status').modal('show');
    } // else closeOptions(a.find('input[name=product_id]').val());

  }
});

function submitCart(form_c) {
  $('#zone-form .select-country .error').remove();
  $.ajax({
    url: 'index.php?route=checkout/cart/submitCart',
    type: 'post',
    data: form_c,
    dataType: 'json',
    beforeSend: function () {// a.button('loading');
    },
    complete: function () {// a.button('reset');
    },
    success: function (json) {
      $('.alert, .text-danger').remove();
      $('.form-group').removeClass('has-error');
      $('.fa-exclamation-circle').remove();

      if (json['error']) {
        if (json['error']['option']) {
          for (i in json['error']['option']) {
            var element = $('#productOptions #input-option' + i.replace('_', '-'));

            if (element.parent().hasClass('input-group')) {//element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
            } else {
              element.parent().addClass('has-error');
            }
          }
        }

        $('.has-error label.control-label').prepend(' <i class="fas fa-exclamation-circle" aria-hidden="true"></i> ');

        if (json['error']['verify']) {
          add2Cart = form_c;
          $('#cart-alert-status').find('.modal-body p').html(json['error']['verify']);
          $('#cart-alert-status').find('.modal-footer').html('<button type="button" id="button-clear-cart" class="button-clear-cart btn btn-lg btn-success" data-dismiss="modal">Yes</button><button type="button" class="btn btn-lg btn-warning" data-dismiss="modal">No</button>');
          $('#cart-alert-status').modal('show');
        } else if (json['error']['stock'] || json['error']['branch_status']) {
          if (json['error']['stock']) {
            $('#restaurant-alert-status').find('.modal-body p').html(json['error']['stock']);
          }

          if (json['error']['branch_status']) {
            $('#restaurant-alert-status').find('.modal-body p').html(json['error']['branch_status']);
          }

          $('#restaurant-alert-status').modal('show');
        } else if (json['error']['location']) {
          if ($('body').hasClass('popup-address') === true) {
            let btnF = $('<button type="button" class="btn btn-success">Najít jinou restauraci</button>'),
                btnC = $('<button type="button" class="btn btn-default">Přidat novou adresu</button>');
            btnF.on('click', function () {
              window.location.href = '/';
            });
            btnC.on('click', function () {
              $('.button-address-bar.clear').trigger('click'); // $('#zone-form .select-country .error').remove();
            });
            let error = $('<div class="error">' + '<p>' + json['error']['location'] + '</p>' + '<p class="buttons"></p>' + '</div>');
            let btns = error.find('p.buttons');
            btns.append(btnC).append(' nebo ').append(btnF);
            $('#zone-form .select-country').append(error);
            add2Cart = form_c;
          } else {
            $('#restaurant-alert-status').find('.modal-body p').html(json['error']['location']);
            $('#restaurant-alert-status').modal('show');
          }
        }
      }

      if (json['success']) {
        if (typeof storeID !== 'undefined') {
          if ($('#restaurant-cart .quantity-adder-name').length < 1) {
            $.ajax({
              url: 'index.php?route=restaurant/cart',
              data: {
                storeID: storeID
              },
              cache: false,
              success: function (data) {
                $('#restaurant-cart-wrap').html(data);
                cart.info();
                viewQty(json['product_id'], json['vendor_id'], json['quantity']);
                cartH();
              }
            });
          } else {
            updatePrdCart(json);
            cart.info();
          }
        } else {
          storeInCart = json['store_href'];
          viewQty(json['product_id'], json['vendor_id'], json['quantity']);
          window.location = storeInCart; // 	return false
        }

        add2Cart = '';
        closeAddressPopup();
      }
    }
  });
}

$(document).delegate('#restaurant-cart button.message, .note-quote span', 'click', function () {
  $('#restaurant-cart .note').removeClass('editing');
  let a = $(this).parents('.quantity-adder-name'),
      b = a.find('.note-editor > span');
  a.find('.note').addClass('editing');
  b.focus();
});
$(document).delegate('.note-editor button.clear', 'click', function () {
  let a = $(this).parents('.quantity-adder-name'),
      b = a.find('.note-editor > span');
  b.text('');
  b.focusin();
});
let keyup_note;
$(document).delegate('.note span[contenteditable=true]', 'keyup', function () {
  if (typeof keyup_note !== 'undefined') keyup_note.abort();
  let a = $(this),
      b = a.parents('.quantity-adder-name'),
      placeHolder = a.attr('data-placeholder');
  keyup_note = $.ajax({
    url: a.attr('data-url'),
    type: "post",
    data: {
      pk: b.attr('data-product-key'),
      value: a.text()
    },
    success: function () {
      let text = a.text();
      if (text != '') text = '** ' + text;
      b.find('.note-quote span').html(text);
    }
  });
});
$(document).delegate('.note span[contenteditable=true]', 'focusout', function () {
  let a = $(this).parents('.quantity-adder-name'),
      b = a.find('.note > span'),
      c = a.find('.note-editor button.clear'),
      d = false; // b.attr('contenteditable','true').focus();

  a.find('.note').removeClass('editing');
  c.click(function (event) {
    if (event.currentTarget === this) d = true;
  });

  if (d === false) {// b.attr('contenteditable','true').focus();
    // a.find('.note').removeClass('editing');
  }
});
/*
$(document).delegate('.text-input','focus mouseup',function(){
	var sel, range;
	var el = $(this)[0];
	if (window.getSelection && document.createRange) { //Browser compatibility
		sel = window.getSelection();
		if(sel.toString() == ''){ //no text selection
			window.setTimeout(function(){
				range = document.createRange(); //range object
				range.selectNodeContents(el); //sets Range
				sel.removeAllRanges(); //remove all ranges from selection
				sel.addRange(range);//add Range to a Selection.
			},1);
		}
	}else if (document.selection) { //older ie
		sel = document.selection.createRange();
		if(sel.text == ''){ //no text selection
			range = document.body.createTextRange();//Creates TextRange object
			range.moveToElementText(el);//sets Range
			range.select(); //make selection.
		}
	}
});*/

$(document).delegate('.text-input', 'keyup', function (event) {
  let a = $(event.target);
  if (a.text() != '') a.parents('.group').addClass('valid');else a.parents('.group').removeClass('valid');
});
$(document).delegate('.text-input', 'focus', function (event) {
  let a = $(event.target),
      placeholder = a.attr('data-placeholder');
  let value = a.text();
  value === placeholder && a.text('');
});
$(document).delegate('.text-input', 'blur', function (event) {
  let a = $(event.target),
      placeholder = a.attr('data-placeholder');
  let value = a.text();
  value === '' && a.text(placeholder); // a.focus()
});
$(document).delegate('.text-input', 'click', function (event) {
  let a = $(this),
      placeholder = a.attr('data-placeholder'),
      text = a.text();
  if (text == placeholder) a.text('').focusin();
});
$(document).delegate('.group button.clear', 'click', function (event) {
  let a = $(this),
      b = a.parents('.group'),
      c = b.find('.text-input');
  c.text('').trigger('keyup').focus();
});
$(document).delegate('.btn-options', 'click', function () {
  let a = $(this).parents('.title-overlay'),
      a_id = a.attr('id');
  if (a.hasClass('show-options') === true) closeOptions(a_id);else openOptions(a_id);
});
$(document).delegate('.button-add-cart', 'click', function () {
  var addcartId = $.trim($(this).attr('id').replace('button-cart-', ''));
  $.ajax({
    url: 'index.php?route=checkout/cart/add',
    type: 'post',
    data: $('#productOptions input[type=\'text\'], #productOptions input[type=\'hidden\'], #productOptions input[type=\'radio\']:checked, #productOptions input[type=\'checkbox\']:checked, #productOptions select, #productOptions textarea'),
    dataType: 'json',
    beforeSend: function () {
      $('.button-add-cart').button('loading');
    },
    complete: function () {
      $('.button-add-cart').button('reset');
    },
    success: function (json) {
      $('.alert, .text-danger').remove();
      $('.form-group').removeClass('has-error');
      $('.fa-exclamation-circle').remove();

      if (json['error']) {
        if (json['error']['option']) {
          for (i in json['error']['option']) {
            var element = $('#productOptions #input-option' + i.replace('_', '-'));

            if (element.parent().hasClass('input-group')) {//element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
            } else {
              element.parent().addClass('has-error');
            }
          }
        }

        $('.has-error label.control-label').prepend(' <i class="fas fa-exclamation-circle" aria-hidden="true"></i> ');

        if (json['error']['stock'] || json['error']['branch_status']) {
          if (json['error']['stock']) {
            $('#restaurant-alert-status').find('.modal-body p').html(json['error']['stock']);
          }

          if (json['error']['branch_status']) {
            $('#restaurant-alert-status').find('.modal-body p').html(json['error']['branch_status']);
          }

          $('#restaurant-alert-status').modal('show');
        }
      }

      if (json['success']) {
        $('#productOptions').modal('hide'); // swal({
        // 	type: "success",
        //          	title: "Success",
        //          	text: 'You have added <span style="color:#EA5B31;font-weight: bold">'+json['product_name']+' </span> to your cart!',
        //          	timer: 2000,
        //          	html: true
        //          	// showConfirmButton: false
        //         	});
        // var scrollBottom = $(window).scrollTop() + window.innerHeight;
        // $('html, body').animate({ scrollTop: scrollBottom }, 'slow');

        if ($('#restaurant-cart .quantity-adder-name').length < 1) {
          $.ajax({
            url: 'index.php?route=restaurant/cart',
            data: {
              storeID: storeID
            },
            cache: false,
            success: function (data) {
              $('#restaurant-cart-wrap').html(data);
              cart.info();
              checkQty(json['product_id'], json['vendor_id']);
              cartH();
            }
          });
        } else {
          updatePrdCart(json);
          cart.info();
        }
      }
    }
  });
});
$(document).delegate('.button-clear-cart', 'click', function () {
  cart.clear();
});
$(document).delegate('.add-down, .add-up', 'click', function () {
  let a = $(this),
      dataUpdate,
      b = a.parents('.quantity-adder-name'),
      key = a.attr('data-key'),
      oldQuantity = parseFloat(a.attr('data-old-quantity')),
      currentQuantity = parseFloat(a.attr('data-current-quantity')),
      step = parseFloat(a.attr('data-step')),
      packageFee = parseFloat(b.attr('data-packaging-fee')),
      price = parseFloat(b.attr('data-price')),
      c = $('#restaurant-cart');
  if (!step) step = 1;
  if (a.hasClass('add-down') === true) currentQuantity = currentQuantity - step;else currentQuantity = currentQuantity + step;
  if (currentQuantity == 0) currentQuantity = step;
  $(".add-down[data-key='" + key + "'], .add-up[data-key='" + key + "']").attr('data-current-quantity', currentQuantity);
  $('#restaurant-cart .packaging-fee .qty-incres > span.current').html('...');
  dataUpdate = {
    'key': key,
    'total': currentQuantity * price,
    'quantity': currentQuantity,
    'packaging-fee-total': currentQuantity * packageFee
  };

  if (typeof c.attr('data-cost-coupon') !== 'undefined') {
    $('#restaurant-cart .total-total-n, #restaurant-cart .total-coupon-n').html('...');
    var tt = {
      'cart_totals': {
        total: {
          value: '...'
        },
        coupon: {
          value: '...'
        }
      }
    };
    const target = {};
    $.extend(true, target, dataUpdate, tt);
    dataUpdate = target;
  }

  b.attr('data-price-total', currentQuantity * price); // let update = cart.update(key,currentQuantity);

  updatePrdCart(dataUpdate);
  cart.info();
  if (typeof editByKeyRequest !== "undefined") editByKeyRequest.abort();
  editByKeyRequest = $.ajax({
    url: 'index.php?route=checkout/cart/editByKey',
    type: 'post',
    data: 'key=' + key + '&quantity=' + (typeof currentQuantity != 'undefined' ? currentQuantity : 1),
    dataType: 'json',
    beforeSend: function () {// $('#cart > button').button('loading');
    },
    complete: function () {// $('#cart > button').button('reset');
    },
    success: function (json) {
      // $.ajax({
      // 	url: 'index.php?route=restaurant/cart',
      // 	cache:false,
      // 	success:function (data) {
      // 		$('#restaurant-cart').html(data);
      // 		cartH();
      // 	}
      // })
      if (json['success']) {
        updatePrdCart({
          'key': key,
          'packaging_fee_cart_calc': json['packaging_fee_cart_calc'],
          'packaging_fee_total': json['packaging_fee_total'],
          'quantity': json['quantity'],
          'cart_totals': json['cart_totals']
        });
        cart.info();
        a.attr('data-old-quantity', json['quantity']);

        if (typeof json['cart_totals'] !== 'undefined') {
          c.attr('data-old-total', json['cart_totals']['total']['value']);
          if (typeof json['cart_totals']['coupon'] !== 'undefined' && typeof json['cart_totals']['coupon']['value'] !== 'undefined') c.attr('data-old-coupon', json['cart_totals']['coupon']['value']);
        }
      } // $('#restaurant-cart').load('index.php?route=restaurant/cart');
      // $('#restaurant-cart-footer').load('index.php?route=common/cart/footerinfo');
      // location.reload();

    },
    error: function (xhr, ajaxOptions, thrownError) {
      if (xhr.status != 0) {
        updatePrdCart({
          'key': key,
          'total': oldQuantity * price,
          'quantity': oldQuantity,
          'packaging-fee-total': oldQuantity * packageFee,
          'cart_totals': {
            total: {
              value: c.attr('data-old-total')
            },
            coupon: {
              value: c.attr('data-old-coupon')
            }
          } // 'packaging_fee_cart_calc':$('#restaurant-cart .packaging-fee .qty-incres > span.old').html()

        });
        cart.info();
        $('#restaurant-cart .packaging-fee .qty-incres > span:not(.old)').html($('#restaurant-cart .packaging-fee .qty-incres > span.old').html());
      }
    }
  });
});
let current_path = base_path = window.location.pathname;
setInterval(function () {
  if (window.location.pathname != current_path) {
    current_path = window.location.pathname;
    let res2 = current_path.split("/");
    let restaurant_id, product_id, product_slug, restaurant_slug;
    let restaurant_path = res2[1];
    let f = restaurant_path.split('.');
    let e = f[0].split('-');
    restaurant_id = e[e.length - 1];
    restaurant_slug = restaurant_path.replace('-' + restaurant_id, '');

    if (typeof res2[3] != 'undefined') {
      let last_path = res2[3].split('.');

      if (typeof last_path[0] != 'undefined') {
        let a = last_path[0],
            b = a.split('-'),
            product_id = b[b.length - 1],
            product_slug = a.replace('-' + product_id, '');
        popup({
          url: 'index.php?route=restaurant/info/product&product_id=' + product_id,
          type: 'product',
          id: product_id
        });
      }
    } else closePopup();
  }
}, 100);
$(document).delegate('.popup > section.product button.close', 'click', function (e) {
  let c_path = window.location.pathname,
      // /restaurant/chut-hanoi-s-sebou-200/81-dimsum-knedlicky-4ks-994.html
  res = c_path.split("/"),
      url = base_path;

  if (typeof res[3] != 'undefined') {
    let patt = new RegExp(".html$"),
        res1 = base_path.split('/');

    if (typeof res1[2] != 'undefined' && typeof res1[1] != 'undefined') {
      if (res1[1] == 'restaurant' && (patt.test(res1[2]) === true || patt.test(res1[3]) === true)) url = 'restaurant/' + res[2] + '.html';
    }

    history.pushState({}, restaurant_meta_title, url);
    document.title = restaurant_meta_title;
  }
});
$(document).delegate('a.load-product', 'click', function (e) {
  e.preventDefault();
  let a = $(this),
      b = a.parents('div.title-overlay'),
      b_id = parseInt(b.attr('data-id')),
      url = a.attr('href'),
      res2 = url.split("/");

  if (typeof res2[res2.length - 1] != 'undefined') {
    let last_path = res2[res2.length - 1];

    if (typeof last_path != 'undefined') {
      popup({
        url: 'index.php?route=restaurant/info/product&product_id=' + b_id,
        loader: b,
        type: 'product',
        id: b_id,
        data: {
          product_path: 'restaurant/' + res2[res2.length - 2] + '/' + last_path
        }
      });
    }
  }
});
$(document).on('focus keyup', '[data-sync]', function (event) {
  var a = $(this),
      a_val;
  var elementType = a.prop('nodeName');
  var target = a.attr('data-sync');
  if (elementType == 'INPUT') a_val = a.val();else a_val = a.text();
  $("[data-sync='" + target + "']").not(a).each(function () {
    var b = $(this);

    if (b.prop('nodeName') == 'INPUT') {
      b.val(a_val);
    } else b.text(a_val);

    b.trigger('change');
  });
});

function share(platform, link) {
  if (platform == 'facebook') {
    if (typeof FB != 'undefined') FB.ui({
      display: 'popup',
      method: 'share',
      href: link
    }, function (response) {});
  } else if (platform = 'twitter') {
    window.open("https://twitter.com/intent/tweet?url=" + link);
  }
} // Autocomplete */


(function ($) {
  $.fn.autocomplete = function (option) {
    return this.each(function () {
      this.timer = null;
      this.items = new Array();
      $.extend(this, option);
      $(this).attr('autocomplete', 'off'); // Focus

      $(this).on('focus', function () {
        this.request();
      }); // Blur

      $(this).on('blur', function () {
        setTimeout(function (object) {
          object.hide();
        }, 200, this);
      }); // Keydown

      $(this).on('keydown', function (event) {
        switch (event.keyCode) {
          case 27:
            // escape
            this.hide();
            break;

          default:
            this.request();
            break;
        }
      }); // Click

      this.click = function (event) {
        event.preventDefault();
        value = $(event.target).parent().attr('data-value');

        if (value && this.items[value]) {
          this.select(this.items[value]);
        }
      }; // Show


      this.show = function () {
        var pos = $(this).position();
        $(this).siblings('ul.dropdown-menu').css({
          top: pos.top + $(this).outerHeight(),
          left: pos.left
        });
        $(this).siblings('ul.dropdown-menu').show();
      }; // Hide


      this.hide = function () {
        $(this).siblings('ul.dropdown-menu').hide();
      }; // Request


      this.request = function () {
        clearTimeout(this.timer);
        this.timer = setTimeout(function (object) {
          object.source($(object).val(), $.proxy(object.response, object));
        }, 200, this);
      }; // Response


      this.response = function (json) {
        html = '';

        if (json.length) {
          for (i = 0; i < json.length; i++) {
            this.items[json[i]['value']] = json[i];
          }

          for (i = 0; i < json.length; i++) {
            if (!json[i]['category']) {
              html += '<li data-value="' + json[i]['value'] + '"><a href="#">' + json[i]['label'] + '</a></li>';
            }
          } // Get all the ones with a categories


          var category = new Array();

          for (i = 0; i < json.length; i++) {
            if (json[i]['category']) {
              if (!category[json[i]['category']]) {
                category[json[i]['category']] = new Array();
                category[json[i]['category']]['name'] = json[i]['category'];
                category[json[i]['category']]['item'] = new Array();
              }

              category[json[i]['category']]['item'].push(json[i]);
            }
          }

          for (i in category) {
            html += '<li class="dropdown-header">' + category[i]['name'] + '</li>';

            for (j = 0; j < category[i]['item'].length; j++) {
              html += '<li data-value="' + category[i]['item'][j]['value'] + '"><a href="#">&nbsp;&nbsp;&nbsp;' + category[i]['item'][j]['label'] + '</a></li>';
            }
          }
        }

        if (html) {
          this.show();
        } else {
          this.hide();
        }

        $(this).siblings('ul.dropdown-menu').html(html);
      };

      $(this).after('<ul class="dropdown-menu"></ul>');
      $(this).siblings('ul.dropdown-menu').delegate('a', 'click', $.proxy(this.click, this));
    });
  };
})(window.jQuery);